<?php
$lang['product_logistc_tlt'] = "Sua mercadoria segura e sempre à vista. ";
$lang['product_logistc_p1'] = "Conheça o GDE Web, ferramenta segura para a emissão e gerenciamento de documentos fiscais, e se precisar outra solução específica, a Finnet oferece para você. Entre em contato e conheça todas as possibilidades para o desenvolvimento de projetos customizados para o seu negócio.";
$lang['product_gde_tlt']='Ferramenta segura para a emissão e gerenciamento de documentos fiscais.';
$lang['product_gde_p1'] = "Composto por uma tecnologia inovadora, o sistema permite a captura, manipulação e endereçamento de qualquer informação através de interfaces simples e integrada. O GDE Web é um produto que faz o gerenciamento de documentos fiscais (NFe,GNRE, MDE e CTe), que garantem a segurança em custódia e gestão de documentos eletrônicos, emitidos ou recebidos, permitindo ainda impressão, consulta, download dos documentos e envio de e-mail para os envolvidos nos processos de transportes da empresa, além de admitir a integração com a Sefaz, o seu ERP e a solução Finnet de
 <a href='./produtos/financeira'>Gerenciamento Financeiro</a> para a automatização do processo de pagamento da GNRE.<br><br>
É garantia do cumprimento de todas as exigências fiscais de transporte de mercadorias. Conheça os módulos do produto GDE Web:";
$lang['product_gde_tlt2'] = "NFe: ";
$lang['product_gde_p2'] = "Módulo responsável pela emissão avulsa ou automática de Notas Fiscais Eletrônicas (NFe) para operações de circulação de mercadorias ou uma prestação de serviço, podendo ser feito através do Webservices ou pela Conectividade (EDI).";
$lang['product_gde_tlt3'] = "GNRE:  ";
$lang['product_gde_p3'] = "Para contribuintes que realizam operações de vendas interestaduais sujeitas à substituição tributária,
esse módulo auxilia na emissão avulsa ou automática de Guia Nacional de Recolhimento de Tributos Estaduais (GNRE).<br><br>
O processo é feito de forma simples e de acordo com a sua necessidade, sendo permitido o preenchimento manual dos dados da guia, envio da NFe ou através de  lote de guias, tudo isso com a utilização do Webservice ou pela Conectividade (EDI).";
$lang['product_gde_tlt4'] ="MDe:";
$lang['product_gde_p4'] ="Módulo que permite aos destinatários realizarem as manifestações das operações de transporte realizadas, podendo ser: ciência da emissão, confirmação da cperação, desconhecimento da operação e operação não realizada. Tudo isso de forma integrada aos envolvidos no processo de transporte de mercadorias.";
$lang['product_gde_tlt5'] ="CTe: ";
$lang['product_gde_p5'] ="Emissão avulsa ou automática para operações de transporte. O módulo Conhecimento de Transporte Eletrônico (CTe) emite documentos para fins fiscais, registrando uma prestação de serviço de transporte de cargas realizada por qualquer modalidades (rodoviário, aéreo, ferroviário, aquaviário e dutoviário) e para diversos produtos e empresas, tudo em um só documento.";
