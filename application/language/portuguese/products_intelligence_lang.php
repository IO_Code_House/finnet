<?php
$lang['intelligence_tlt1'] ="Relatórios Analíticos";
$lang['intelligence_tlt1_'] ="A missão da Finnet é entregar soluções que complementem e facilitem os processos da sua empresa, independente do seu porte ou segmento.";
$lang['intelligence_p1'] = "Para isso, desenvolve sistemas exclusivos a partir das suas necessidades, integrando ferramentas e softwares inovadores
 que garantem os melhores resultados, como o sistema de BI: <b>o Business Intelligence</b>.";
$lang['intelligence_p2'] ="Um software único, criado para auxiliar na análise e tomada de decisões do seu negócio. <br>
Por ser uma ferramenta totalmente personalizada, se adapta perfeitamente ao modelo da sua empresa.";
$lang['intelligence_p4'] ="Conheça mais a fundo esta inovação da Finnet.";
$lang['intellicence_link_1'] ="Business Intelligence";
$lang['intelligence_p5'] ="Caso deseje uma outra solução específica, entre em contato com a Finnet e conheça todas as possibilidades para o desenvolvimento de projetos customizados para o seu negócio. ";
$lang['intellicence_link_2']="Contato";
$lang['intelligence_tlt2']="BUSINESS INTELLIGENCE";
$lang['intelligence_sub_tlt2']="Uma inteligência Finnet que se adapta à inteligência da sua empresa.";
$lang['intelligence_p5']="Diferente de um software pronto, antes da adesão do Business Intelligence, a Finnet faz um serviço consultivo com o cliente, tendo como objetivo entender a real necessidade do comprador da solução, através das informações recebidas é desenvolvido uma inteligência dentro da ferramenta para que as informações saiam, em tempo real, conforme a necessidade de cada empresa.";
$lang['intelligence_p6']="Um sistema de grande importância no apoio à tomada de decisão, o Business Intelligence é uma ferramenta personalizada a necessidade de cada cliente, serve de subsídio na construção de análises gerenciais, permitindo o cruzamento de informações e relatórios analíticos, além da projeção de dados através do histórico da empresa.";
$lang['intelligence_p7']="Nesta ferramenta estão presentes os conceitos de OLAP (on line analytical processing), Data Mining (garimpagem de dados) e Drill-down (penetração na informação e busca de sua composição). Sua interface de geração de cubos para a análise gerencial é simples e de fácil compreensão.";
