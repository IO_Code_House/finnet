<?php
$lang['career_tlt'] = "Paixão por inovação. ";
$lang['career_p1'] = "É isso que está presente no DNA de um colaborador Finnet.<br>
    Seja no âmbito profissional, buscando superar e inovar com qualidade nas entregas e prazos, como no âmbito socioambiental, se preocupando como suas atitudes vão refletir no mundo em que vive e, até mesmo no âmbito pessoal, estando disposto a aprender e crescer, correndo atrás de desafios para evoluir sua carreira e a empresa na qual trabalha.";
$lang['career_p2'] = "Se você também tem o DNA Finnet, venha conhecer nossos programas e nosso modo de trabalhar e pensar. Estamos sempre em busca de novos talentos.";
$lang['career_p3'] = "Cadastre o seu currículo para conferir as oportunidades de emprego e boa sorte.";
$lang['career_p4'] = "A família Finnet está esperando por você. ";
$lang['development'] = "Programa de desenvolvimento";
$lang['development_p1'] = "Os desafios do dia a dia são essenciais para o crescimento profissional de qualquer colaborador.<br>
    Mas a Finnet vai além disso. Com a valorização da experiências cotidianas de seus colaboradores, a Finnet se preocupa em investir no potencial e evolução de cada um.<br>
    Para isso, investe em ações e projetos que têm como objetivo aprimorar conhecimentos e habilidades, engajar pessoas e fazer florescer o que há de melhor em seus profissionais.";
$lang['development_p2'] = "Para entender melhor as iniciativas da Finnet, conheça algumas de suas práticas abaixo. ";
$lang['development_tab1'] = "FORMAÇÃO ACADÊMICA";
$lang['development_tab1_txt1'] = "A formação de um bom profissional, começa com uma boa formação educacional.";
$lang['development_tab1_txt2'] = "Pensando nisso, a  Finnet possui parcerias com faculdades e universidades em São Paulo, fornecendo descontos em mensalidades de cursos superiores e de extensão.";
$lang['development_tab2'] = "TREINAMENTOS";
$lang['development_tab2_txt1'] = "Colaboradores em constante evolução. A Finnet visa aprimorar o conhecimento de todos os seus colaboradores, promovendo e financiando treinamentos destinados às diferentes áreas, com o objetivo de desenvolver habilidades, conhecimento de mercados e dos produtos da empresa, e até mesmo com trabalhos de desenvolvimento pessoal do colaborador.";
$lang['development_tab3'] = "INCENTIVO À RETENÇÃO DE TALENTOS";
$lang['development_tab3_txt1'] = "A Finnet acredita e preza muito a valorização de bons profissionais. ";
$lang['development_tab3_txt2'] = "Por isso, além de retribuí-los traçando planos de carreira de acordo com seus objetivos e ambições, ainda presenteia aqueles que já estão há algum tempo nesta parceria: quem completa 5 anos de empresa, recebe uma viagem para qualquer lugar do Brasil com acompanhante. E os que comemoram 10 anos, vão para os EUA em Melbourne - Flórida,  com direito a acompanhante e um passeio completo no parque da Disney.";
$lang['development_tab4'] = "PROGRAMA DE RECONHECIMENTO";
$lang['development_tab4_txt'] = "Reconhecer para manter. Esse é o principal pensamento para uma empresa que deseja preservar profissionais bons e satisfeitos.<br>
    Assim, na Finnet, o profissional que conquista durante o ano o destaque na dedicação, inovação, superação,qualidade, ética, sustentabilidade é presenteado com uma viagem, de 5 dias, em um resort all inclusive com acompanhante.<br>
    Além da valorização da empresa, a Finnet também valoriza o reconhecimento entre colaboradores.<br>
    Desta forma, criou o programa de incentivo Valeu! Um espaço onde os funcionários podem agradecer e elogiar colegas por projetos executados ou contribuição recebida. Quem é reconhecido pode ganhar diversos prêmios, como: cinema, jantar, show, final de semana em um hotel fazenda e até 50% do salário em bonificação, além disso, todas as premiações de entretenimento têm o direito de levar um acompanhante";

$lang['culture_tlt']="A valorização como profissional e como pessoa. ";
$lang['culture_p1']="É assim que a Finnet construiu a sua família de colaboradores. Sempre buscando meios de contribuir para a evolução profissional de quem trabalha na empresa, mas sem esquecer da importância da qualidade de vida e relações interpessoais entre colegas e familiares.";
$lang['culture_p2']="É por este motivo que a Finnet incentiva momentos de relaxamento, descontração e interação em espaços dedicados aos colaboradores. Além disso, promove ações e eventos que contam com o envolvimento da família e amigos no dia a dia do profissional.";
$lang['culture_p3']="Porque a satisfação de um colaborador é o ingrediente principal do sucesso de uma empresa.";
$lang['curriculum_tlt'] = "Vagas";
$lang['curriculum_p1'] = "Integrar equipes vencedoras, superar desafios, crescimento profissional é o que desejamos";
$lang['curriculum_p2'] = "Se você se identificou com a nossa cultura e valores, venha fazer parte do nosso time.";
$lang['curriculum_link'] = "Cadastre aqui seu currículo.";
