<?php
$lang['contact_p1'] = "A Finnet está aguardando o seu contato para conectar as melhores soluções às suas maiores necessidades. ";
$lang['contact_p2'] = "Sugestões, dúvidas, informações sobre a empresa ou produtos, suporte, serviços qualquer outra informação que você necessite, fique à vontade para entrar em contato com a Finnet pelo canal que desejar. ";
$lang['contact_p3'] = "ATENDIMENTO POR E-MAIL";
$lang['commercial'] = "Comercial";
$lang['thanks_ask_other'] = "Elogios, reclamações e sugestões";
$lang['support_commercial'] = "Comercial";
$lang['support_commercial_p1'] = "Atendimento das 8h as 18h, de 2a a 6a.";
$lang['phone_support'] = "ATENDIMENTO POR TELEFONE";
$lang['chat_support'] = "ATENDIMENTO POR CHAT";
$lang['support_support'] = "Suporte técnico";
$lang['support_support_p1'] = "Atendimento 24hs por dia, 7 dias por semana. ";
$lang['tech_suport'] = "Atendimento técnico";
