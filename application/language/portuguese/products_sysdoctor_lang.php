<?php
$lang['sysdoctor_p1'] = "A Finnet cuida da gestão do seu negócio para você cuidar ainda melhor de seus pacientes.";
$lang['sysdoctor_p2'] = "Já imaginou uma ferramenta exclusiva para otimizar o dia a dia da sua clínica ou consultório médico? A Finnet não só imaginou como já desenvolveu.<br>
O software de gestão médica da Finnet possui recursos criados para facilitar a rotina de trabalho, permitindo configurar a agenda de cada médico, enviar e-mails para pacientes, controlar a fila de espera, o prontuário eletrônico, acessar a base de apoio para o diagnóstico e ainda cadastrar convênios atendidos.";
$lang['sysdoctor_p3'] = "A solução ideal e com o melhor custo-benefício para você, profissional da área de saúde.";
$lang['sysdoctor_p4'] = "Conheça todas as vantagens do Sysdoctor";
$lang['sysdoctor_chang1'] = 'Um Software de gestão especializado para a sua clínica e consultório médico. <br><br>
O Sysdoctor oferece automatização nos processos administrativos da área médica, tendo como principal objetivo trazer facilidade na rotina diária do consultório, otimizando
custos e melhorias no processo de atendimento aos pacientes.';
$lang['sysdoctor_p5'] ="A solução é totalmente desenvolvida em plataforma web, configurada em alta disponibilidade, seguindo as boas práticas de segurança do mercado.";
$lang['sysdoctor_p6'] ="Isto possibilita que todas as funcionalidades estejam disponíveis a qualquer hora e lugar, de forma totalmente segura." ;
$lang['sysdoctor_p7']="Além do Software de Gestão, o Sysdoctor ainda possui serviços destinados à administração da clínica e consultório médico";
$lang['sysdoctor_p8']="Para saber mais sobre os serviços e valores,  acesse o site do produto:";