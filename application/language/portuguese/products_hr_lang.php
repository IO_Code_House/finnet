<?php
$lang['hr_title1'] = 'Controle e segurança em empréstimos consignados.';
$lang['hr_p1'] = 'A Finnet desenvolveu um produto que auxilia   empresas que oferecem o benefício do empréstimo consignado aos funcionários.';
$lang['hr_p3'] = 'Conheça melhor a ferramenta que facilita a gestão do empréstimo consignado';
$lang['hr_chang1'] = 'Caso sua empresa tenha a necessidade de algum outro tipo de solução, conheça os nossos projetos customizados, desenvolvidos especificamente para atender a sua demanda.';
$lang['hr_chang2'] = 'Entre em contato com a Finnet';
$lang['hr_title2'] = 'Simplicidade na gestão do benefício aos seus colaboradores.';
$lang['hr_p5'] = 'O Portal Consignado possibilita a realização do cálculo da margem consignável e/ou confirmação da averbação de contratos, além da conciliação das parcelas do benefício aos seus funcionários.';
$lang['hr_p6'] = 'Por meio dessa solução, sua empresa poderá gerenciar, na totalidade, as atividades relacionadas ao benefício, além de permitir fazer pesquisas por meio de multi-filtros e obter relatórios com documento na extensão mais adequada, PDF e/ou XLS.';
