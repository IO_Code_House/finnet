<?php
$lang['clients_tlt'] = "Nosso sucesso vem da sua satisfação. ";
$lang['clients_p1'] = "Nosso sucesso vem da sua satisfação. ";
$lang['clients_p2'] = "A Finnet enxerga seus clientes como uma relação de parceria que nunca deve parar de crescer. Do nosso lado, estudamos e desenvolvemos constantemente produtos e serviços para garantir que isso aconteça. E do lado de nossos parceiros, recebemos a credibilidade e satisfação que buscamos.";