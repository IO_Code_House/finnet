<?php
$lang['products_tlt'] = "Segurança e inteligência que acompanham cada etapa da gestão financeira da sua empresa.";
$lang['products_p1'] = "Os produtos da Finnet direcionados à gestão Financeira, foram desenvolvidos  para aumentar a eficiência operacional e diminuir os riscos com erros manuais durante o processo de trabalho. Com o objetivo de guiar a sua empresa desde a geração de documentos eletrônicos, até a entrega e gerenciamento dos processos de finanças, a Finnet oferece sistemas que são referência no mercado, homologados nas principais instituições financeiras, podendo ser integrados a um ERP.";
$lang['products_p2'] = "Conheça melhor os produtos desenvolvidos para facilitar o dia a dia da gestão financeira do seu negócio:";
$lang['products_list_tlt1'] = "Gerenciamento Financeiro";
$lang['products_list_link'] = "Saiba Mais";

$lang['supplier_panel_nav'] = "Painel do Fornecedor";
$lang['supplier_panel'] = "Painel<span>Fornecedor</span>";
$lang['products_list_p1'] = "Soluções destinadas ao gerenciamento de toda a cadeia financeira da empresa.";
$lang['products_list_p2'] = "Automatização da prestação de contas dos colaboradores.";
$lang['products_list_supplier_p'] = "Sistema dedicado para a gestão da prestação de contas da empresa.";
$lang['products_list_portal_p'] = "Plataforma de cobrança, através da disponibilização de boletos para pagamentos.";
$lang['products_list_portal_p2'] = "Procurando outra solução específica? A Finnet oferece para você. Entre em contato e conheça todas as possibilidades para o desenvolvimento de projetos customizados para o seu negócio.";
$lang['portal_slips_nav'] = "Portal de boletos";
$lang['management'] = "Gerenciamento";
$lang['management_tlt'] = "GERENCIAMENTO FINANCEIRO";
$lang['management_p1'] = "Soluções exclusivas que se adaptam ao gerenciamento de toda a cadeia financeira e operações em diversos bancos.<br>
Com   as soluções de Gerenciamento Financeiro, você otimiza sua rotina de trabalho, desde a geração de documentos eletrônicos, até a respectiva
entrega, consulta e gestão dos processos e informações financeiras. É muito mais facilidade para seus funcionários e mais segurança para você.";
$lang['management_p2'] = "Conheça   os módulos da solução de Gerenciamento Financeiro   :";
$lang['management_pay_p1'] = "Pagamentos: ";
$lang['management_pay_p2'] = "Com o módulo de Pagamentos, a segurança e agilidade da gestão de contas a pagar da sua empresa está garantida. A partir dele, você é capaz de realizar o cadastro e manutenção de pagamentos aos fornecedores, de tributos e até mesmo de salários. A autorização de pagamentos antes do envio dos compromissos para agendamento junto à instituição financeira também pode ser feito através do módulo.";
$lang['management_pay_p3'] = "Para garantir ainda mais segurança, é possível fazer a configuração dos autorizadores ou configuradores, de
acordo com as alçadas de pagamento estipuladas nas procurações empresariais, e também disponibilizar os comprovantes e protocolos de pagamentos
 da empresa.";
$lang['management_collection_p1'] = "Cobrança: ";
$lang['management_collection_p2'] = "O objetivo deste módulo é auxiliar na gestão de contas a receber, facilitando o processo e tornando-o mais automático. Por meio do módulo de cobrança você pode fazer o cadastro e manutenção de títulos de cobrança, imprimir boletos e disponibilizar relatórios gerenciais para acompanhamento da carteira de cobrança.";
$lang['management_bar_code_p1'] = "Arrecadação de Contas: ";
$lang['management_bar_code_p2'] = "A arrecadação diária da sua empresa agora pode contar com um módulo de gestão avançado. Com ele, é possível obter informações do ranking de arrecadações, dados de tarifas e arrecadações realizadas, podendo ser via código de barras ou débito automático.";
$lang['management_extract_p1'] = "Extrato: ";
$lang['management_extract_p2'] = "Muito mais facilidade e organização para os extratos e saldos consolidados das suas contas em todos os bancos. O módulo Extrato permite que o seu controle seja feito através de filtros de pesquisa: os lançamentos podem ser acompanhados e filtrados por banco, conta corrente, convênio, lançamento (débito/crédito), histórico e período. E todas as funcionalidades são realizadas de acordo com a sua necessidade gerencial.";
$lang['refund'] = "Reembolso";
$lang['refund_tlt'] = "REEMBOLSO DE DESPESA";
$lang['refund_p1'] = "Eficiência e comodidade na prestação de contas dos colaboradores da sua empresa.";
$lang['refund_p2'] = "Facilidade onde quer que você esteja: com  o produto Reembolso de Despesa, é possível fazer a conferência e efetivação da restituição dos consumos corporativos dos seus funcionários e ainda solicitar um novo reembolso remotamente, pelo seu computador ou celular, assim como a aprovação e liberação dos pagamentos ao banco. Além disso, o sistema admiti configurar as alçadas de aprovadores, conforme a regra corporativa de cada empresa.";
$lang['supplier_p1'] = "Facilidade integrada: o status de pagamento de todos os seus fornecedores em um únicolugar.";
$lang['supplier_p2'] = "Com este aplicativo web, a rotina de contas a pagar da sua empresa vai ficar muito mais rápida e fácil. Em um único painel, seus fornecedores acessam os status de pagamentos de suas contas, desde a programação da Nota Fiscal até a sua efetivação, além de visualizar os comprovantes e protocolos de pagamentos já realizados.";
$lang['supplier_p3'] = "E você consegue gerenciar o processo de prestação de contas, de uma forma simples e prática, possibilitando o controle do ciclo de pagamentos, junto ao banco, emissão dos comprovantes de pagamentos e geração de relatórios gerenciais.<br><br>Fornecedores e empresas conectados em uma única ferramenta";
$lang['portal_slips_tlt'] = "Portal de <span>Boletos</span>";
$lang['portal_slips_p1'] = "Acesso aos boletos de pagamento sem burocracia nem demora.";
$lang['portal_slips_p2'] = "O Portal de boletos é um produto que  facilita o processo de cobrança da sua empresa. Através do sistema é possível que seus clientes visualizem e imprimam  todos os boletos disponíveis para pagamento, antes mesmo do recebimento do boleto  físico. Além disso, permite a personalização da sua cobrança, realizando automaticamente a atualização dos valores, em caso de descontos ou abatimentos e incidência de juros e multa.";
$lang['portal_slips_p3'] = "E você também pode visualizar todos os boletos disponíveis e conseguir identificar os que já foram acessados por seus clientes. Além de admitir o envio de boletos por e-mail ou à gráfica para impressão, atendendo a necessidade de cada empresa, e fazer o acompanhamento da carteira de cobrança.";

