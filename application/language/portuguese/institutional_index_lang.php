<?php
$lang['who_we'] = "Quem Somos";
$lang['our_strengths'] = "Nossas Fortalezas";
$lang['ecology'] = "sou eco";
$lang['units'] = "unidades";
$lang['who_she_tlt'] = "Inovar para simplificar.";
$lang['who_she_p1'] = "Esse é o pensamento que guia cada solução que desenvolvemos para a sua empresa.";
$lang['who_she_p2'] = "Há mais de 12 anos, a empresa de tecnologia Finnet trabalha integrando profissionais de alta qualidade a ferramentas e softwares exclusivos com um único objetivo: facilitar todo processo de gestão de forma inteligente e simples. Bancos, consultórios, escritórios, setor logístico: quando nasce uma necessidade para a sua empresa, independente do porte ou segmento, nasce uma solução tecnológica Finnet. O nosso conhecimento e reconhecimento auxiliam no dia a dia do seu negócio, integrando sistemas inovadores e extremamente seguros, para alcançar a otimização de processos e redução de custos de acordo com o que você busca.";
$lang['who_she_p3'] = "Estamos ansiosos para conectar à nossa expertise ao sucesso da sua empresa.";
$lang['our_strengths_tlt'] = "Nossas Fortalezas";
$lang['our_strengths_p1'] = "A Finnet tem como missão desenvolver as melhores tecnologias e reter os melhores profissionais, para assim conseguir conquistar resultados sustentáveis aos seus clientes.";
$lang['our_strengths_p2'] = "Para isso, trabalha em harmonia com a natureza, ética, qualidade e o máximo de inovação, se superando a cada dia. <br>
    Com mais 12 anos de experiência, a Finnet está interligada a mais de 40 instituições financeiras e 25 mil empresas. Além disso, investe constantemente em sua estrutura para garantir o armazenamento de dados em Data Centers de última geração e o máximo de proteção. <br>
    A Finnet está sempre evoluindo e conquistando novas certificações e prêmios que afirmam o seu comprometimento e reconhecimento em um mercado tão competitivo.";
$lang['our_strengths_cta'] = "CONFIRA A LISTA COMPLETA DE PRÊMIOS ›";
$lang['our_strengths_p3'] = 'E é este equilíbrio entre inovação, confiabilidade e solidez que garante o sucesso da Finnet e de seus clientes';
$lang['ecology_tlt'] = "Sou Eco";
$lang['ecology_p1'] = "Mais do que ter consciência. Fazer a diferença.";
$lang['ecology_p2'] = "É nisso que a Finnet acredita quando o assunto é evolução. Seja no aspecto econômico, ambiental ou nas relações entre pessoas e suas ações.";
$lang['ecology_p3'] = "E para fazer acontecer, insere no dia a dia projetos e ações que valorizam seu interior, colaboradores e processos, e seu exterior: o ambiente no qual vivemos.";
$lang['ecology_cta'] = "CONHEÇA O PROJETO SOU ECO E O MUNDO SUSTENTÁVEL FINNET ›";
$lang['units_tlt'] = "Nossas Unidades";
$lang['units_p1'] = "Para alcançar resultados cada vez mais assertivos, a Finnet dividiu suas frentes de atuação em diferentes filiais. Dessa forma, é possível ter um foco maior e mais especializado em cada um de nossos clientes para garantir sua satisfação.";
$lang['units_p2'] = "Confira no mapa as localizações das nossas 5 unidades no Brasil e a subsidiária, presente nos EUA.";
$lang['melbourne_text']='A filial localizada nos EUA oferece todos os serviços financeiros disponíveis no Brasil,
               adequado ao sistema local, no qual gera oportunidade de troca de tecnologia e inovação nos
               produtos e serviços oferecidos na carteira.';
$lang['localization'] = 'Localização';
$lang['sao_paulo_text']='O coração da empresa está na filial localizada na Paulista, onde estão dedicadas
               as frentes de desenvolvimento, inovação,
               qualidade, inteligência, comercialização, marca e posicionamento de mercado.';

$lang['barueri_text'] = 'A filial de Alphaville dedica-se 100% ao atendimento da Finnet, com profissionais
               dedicados a promover um excelente relacionamento com os clientes.';
$lang['ribeirao_preto_text'] = ' Pensando na concentração de indústrias na região, a filial de Ribeirão Preto atua na comercialização de soluções
               e produtos da empresa.';
$lang['sje_text'] = 'Pensando na concentração de indústrias na região, a filial de São José atua na busca de parceiros tecnológicos
               e na comercialização de soluções e produtos da empresa.';
$lang['rio_grande_text'] = 'Pensando na concentração de indústrias na região, a filial do Rio Grande do Sul atua na comercialização de
                soluções e produtos da empresa.';
