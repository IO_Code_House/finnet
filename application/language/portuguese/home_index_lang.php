<?php
$lang['home_category_tlt'] = "Quando nasce uma necessidade para a sua empresa, nasce uma solução ";
$lang['home_category_p'] = "Sistemas completos de gerenciamento que agregam inteligência ao seu negócio. ";
$lang['testimonial_tlt'] = "A evolução de nossos clientes é a nossa maior recompensa.";