<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institutional extends MY_Controller{

    public function index()
    {
        $this->setPageTitle(lang('institutional'));
        
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('institutional');
        $this->data['banner']->img = './assets/img/institutional/banner-1.jpg';

        $this->renderer();
    }
}
