<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends MY_Controller{

    public function index()
    {
        $this->setPageTitle(lang('careers'));
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('careers');
        $this->data['banner']->img = './assets/img/career/banner-1.jpg';
        $this->renderer();
    }
}
