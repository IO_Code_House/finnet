<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller
{

    public function index()
    {
        $this->setPageTitle(lang('contact'));
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('contact');
        $this->data['banner']->img = './assets/img/contact/banner-1.jpg';
        $this->renderer();
    }

    public function send()
    {
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $data = $this->input->post();

            $this->load->model('contact_model');

            $inserted = $this->contact_model->insert($data, true);

            if ($inserted) {

                $this->load->library('email');

                $message = '<strong>Empresa</strong>: ' . $data['company'] . '<br>' . PHP_EOL;//
                $message .= '<strong>Nome</strong>: ' . $data['name'] . '<br>' . PHP_EOL;
                $message .= '<strong>Email</strong>: ' . $data['email'] . '<br>' . PHP_EOL;
                $message .= '<strong>Telefone</strong>: ' . $data['phone'] . '<br>' . PHP_EOL;
                $message .= '<strong>Cidade</strong>: ' . $data['city'] . '<br>' . PHP_EOL;
                $message .= '<strong>Estado</strong>: ' . $data['state'] . '<br>' . PHP_EOL;
                $message .= '<strong>Telefone</strong>: ' . $data['phone'] . '<br>' . PHP_EOL;
                $message .= '<strong>Motivo</strong>: ' . $data['reason'] . '<br>' . PHP_EOL;
                $message .= '<strong>Mensagem</strong>: ' . $data['mesage'] . '<br>' . PHP_EOL;

                $this->email->from($this->email->smtp_user, $data['name']);
                $this->email->reply_to($data['email'], $data['name']);
                $this->email->to($this->email->smtp_user);
                $this->email->bcc(array('sena@iocode.com.br'));
                $this->email->subject(base_url() . ' - Contato site');
                $this->email->message($message);
                $this->email->set_alt_message(strip_tags($message));

                if (!$this->email->send()) {
                    $this->setError(lang('generic_error'));
                    log_message('debug', __METHOD__ . var_export($this->email->print_debugger(), TRUE));
                } else {
                    $this->setMsg(lang('email_sent'));
                }
            } else {
                $this->setError(lang('generic_error'));
            }
        } else {
            $this->setError(lang('generic_error'));
        }
        redirect($this->uri->segment(1));
    }
}
