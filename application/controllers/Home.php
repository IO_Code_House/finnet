<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function index()
    {
        $this->loadJs(array('name' => 'full_banner'));
        $this->loadCss(array('name' => 'full_banner'));



        $this->load->model('testimonial_model');
        $this->data['testimonial'] = $this->testimonial_model->get()->result();

        $this->load->model('banner_model');
        $this->data['banner'] = $this->banner_model->get()->result();
        $this->load->helper('banner');
        parse_banner($this->data['banner'], $this->data['lang']);

        $this->load->helper('testimonial');
        parse_testimonial($this->data['testimonial'], $this->data['lang']);

        $this->renderer();
    }
}

