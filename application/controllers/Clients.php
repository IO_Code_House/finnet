<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller{

    public function index()
    {
        $this->setPageTitle(lang('our_clients'));
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('our_clients');
        $this->data['banner']->img = './assets/img/clients/banner-1.jpg';

        $this->load->model('client_model');
        $this->data['client'] = $this->client_model->get()->result();

        $this->renderer();
    }
}
