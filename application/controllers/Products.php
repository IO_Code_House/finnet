<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller
{

    public function index()
    {
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('products_solutions');
        $this->data['banner']->subTitle = lang('financial_management');
        $this->data['banner']->img = './assets/img/products/banner-1.jpg';
        $this->setPageTitle($this->data['banner']->subTitle);
        $this->renderer();
    }

    public function logistic()
    {

        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('products_solutions');
        $this->data['banner']->subTitle = lang('logistic_management');
        $this->data['banner']->img = './assets/img/products/banner_logistc.jpg';
        $this->setPageTitle($this->data['banner']->subTitle);
        $this->renderer();
    }

    public function sysdoctor()
    {

        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('products_solutions');
        $this->data['banner']->subTitle = lang('health');
        $this->data['banner']->img = './assets/img/products/banner_sysdoctor.jpg';
        $this->setPageTitle($this->data['banner']->subTitle);
        $this->renderer();
    }

    public function intelligence()
   {
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('products_solutions');
        $this->data['banner']->subTitle = lang('analytical_reports');
        $this->data['banner']->img = './assets/img/products/intelligence-banner.jpg';
        $this->setPageTitle($this->data['banner']->subTitle);
        $this->renderer();
    }

    public function service()
    {
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('products_solutions');
        $this->data['banner']->subTitle = lang('service');
        $this->data['banner']->img = './assets/img/products/services-banner.jpg';
        $this->setPageTitle($this->data['banner']->subTitle);
        $this->renderer();
    }

    public function hr()
    {
        $this->setPageTitle(lang('human_resource_management_department'));
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('products_solutions');
        $this->data['banner']->subTitle = lang('human_resource_management_department');
        $this->data['banner']->img = './assets/img/products/services-hr.jpg';
        $this->renderer();
    }
}
