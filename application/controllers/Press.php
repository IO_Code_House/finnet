<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Press extends MY_Controller{

    public function index()
    {
        $this->setPageTitle(lang('press'));
        $this->data['banner'] = new stdClass();
        $this->data['banner']->title = lang('press');
        $this->data['banner']->img = './assets/img/press/banner-1.jpg';

        $this->load->model('release_model');
        $this->load->model('midia_model');
        $this->load->helper('release');
        $this->load->helper('midia');

        $this->data['release'] = $this->release_model->get()->result();
        $this->data['midia'] = $this->midia_model->get()->result();

        parse_midia($this->data['midia'], $this->data['lang']);
        parse_release($this->data['release'], $this->data['lang']);

        $this->renderer();
    }
}
