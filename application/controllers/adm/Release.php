<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class release
 */
class Release extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('release_model');
    }
    public function index(){
        $this->data['list'] = $this->release_model->get()->result();

        $this->load->helper('release');
        parse_release($this->data['list'], $this->data['lang']);

        $this->loadJs(array(
            array(
                'name' => 'jquery.dataTables.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables',
                'path' => 'assets/dataTables/'
            )
        ));

        $this->loadCss(array(
            array(
                'name' => 'jquery.dataTables',
                'path' => 'assets/dataTables/media/css/'
            )
        ));
        parent::renderer();
    }
    public function edit($id = NULL){

        if($this->uri->segment(3) == 'editar' && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }elseif($id > 0){
            $data = $this->release_model->get(array('id' => $id))->result();
            if(count($data) > 0){
                $data = current($data);
                $data->title = unserialize($data->title);
                $data->subtitle = unserialize($data->subtitle);
                $this->data['data'] = $data;
            }
        }

        parent::renderer();
    }
    public function record($id = NULL){
        $id = (int)$id;
        if($this->input->post()){
            $data = array(
                'title' => serialize($this->input->post('title')),
                'subtitle' => serialize($this->input->post('subtitle')),
                'link' => $this->input->post('link')
            );

            if($id === 0){
                $id = $this->release_model->insert($data, true);
            }else{
                $this->release_model->update(array('id' => $id), $data);
            }
            if($id === 0) {
                $this->setError(lang('error_saving'));
            }else{
                $this->setMsg(lang('saved_successfully'));
            }
        }else{
            $this->setError(lang('generic_error'));
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
    public function delete($id)
    {
        $this->release_model->delete(array('id' => $id));
        $this->setMsg(lang('deleted'));
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}