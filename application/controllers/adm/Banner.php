<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class banner
 */
class Banner extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('banner_model');
    }
    public function index(){
        $this->data['list'] = $this->banner_model->get()->result();

        $this->load->helper('banner');
        parse_banner($this->data['list'], $this->data['lang']);

        $this->loadJs(array(
            array(
                'name' => 'jquery.dataTables.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables',
                'path' => 'assets/dataTables/'
            )
        ));

        $this->loadCss(array(
            array(
                'name' => 'jquery.dataTables',
                'path' => 'assets/dataTables/media/css/'
            )
        ));
        parent::renderer();
    }
    public function edit($id = NULL){

        if($this->uri->segment(3) == 'editar' && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }elseif($id > 0){
            $data = $this->banner_model->get(array('id' => $id))->result();
            if(count($data) > 0){
                $data = current($data);
                $data->title = unserialize($data->title);
                $data->subtitle = unserialize($data->subtitle);
                $this->data['data'] = $data;
            }
        }

        parent::renderer();
    }
    public function record($id = NULL){
        $id = (int)$id;
        if($this->input->post()){
            $data = array(
                'title' => serialize($this->input->post('title')),
                'subtitle' => serialize($this->input->post('subtitle')),
                'link' => $this->input->post('link'),
                'img' => $this->input->post('img')
            );

            if($_FILES['img']['size'] > 0) {

                if($id !== 0){
                    $tempVar = $this->banner_model->get(array('id' => $id))->result();
                    $tempVar = current($tempVar);
                    @unlink($tempVar->img);
                }
                $uploadPath = './assets/upload/' . $this->router->class . '/';
                if (!is_dir($uploadPath)) {
                    mkdir($uploadPath, DIR_WRITE_MODE, TRUE);
                }
                $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png', 'encrypt_name' => true));

                if ( ! $this->upload->do_upload('img')) {
                    $this->setError($this->upload->display_errors());
                }else{
                    $file = $this->upload->data();
                    $data['img'] = $uploadPath . $file['file_name'];

                    $this->load->library('image_lib', array(
                        'source_image' => $data['img'],
                        'maintain_ratio' => TRUE,
                        'width' => 1920,
                        'height' => 867
                    ));
                    if ( ! $this->image_lib->resize())
                    {
                        $this->setError($this->image_lib->display_errors());
                    }
                }
            }
            if($id === 0){
                $id = $this->banner_model->insert($data, true);
            }else{
                $this->banner_model->update(array('id' => $id), $data);
            }
            if($id === 0) {
                $this->setError(lang('error_saving'));
            }else{
                $this->setMsg(lang('saved_successfully'));
            }
        }else{
            $this->setError(lang('generic_error'));
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
    public function delete($id)
    {
        $tempVar = $this->banner_model->get(array('id' => $id))->result();
        $tempVar = current($tempVar);
        @unlink($tempVar->img);

        $this->banner_model->delete(array('id' => $id));
        $this->setMsg(lang('deleted'));
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}