<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class contact
 */
class Contact extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('contact_model');
    }
    public function index(){
        $this->data['list'] = $this->contact_model->get()->result();

        $this->loadJs(array(
            array(
                'name' => 'jquery.dataTables.min',
                'path' => 'assets/dataTables/media/js/'
            ),
            array(
                'name' => 'dataTables',
                'path' => 'assets/dataTables/'
            )
        ));

        $this->loadCss(array(
            array(
                'name' => 'jquery.dataTables',
                'path' => 'assets/dataTables/media/css/'
            )
        ));
        parent::renderer();
    }
    public function edit($id = NULL){

        if($this->uri->segment(3) == 'editar' && (int) $id === 0) {
            redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) . '/novo');
        }elseif($id > 0){
            $data = $this->contact_model->get(array('id' => $id))->result();
            if(count($data) > 0){
                $data = current($data);
                $this->data['data'] = $data;
            }
        }
        parent::renderer();
    }
    public function record($id = NULL){
        $id = (int)$id;
        if($this->input->post()){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]');

            if($this->form_validation->run() === FALSE){
                $this->setError(validation_errors());
                if($id === 0){
                    $redirect = '/novo';
                }else{
                    $redirect = '/editar/' . $id;
                }
                redirect($this->uri->segment(1) . '/' . $this->uri->segment(2) .  $redirect);
            } else {
                $data = $this->input->post();

                if($id === 0){
                    $id = $this->contact_model->insert($data, true);
                }else{
                    $this->contact_model->update(array('id' => $id), $data);
                }
                if($id === 0) {
                    $this->setError(lang('error_saving'));
                }else{
                    $this->setMsg(lang('saved_successfully'));
                }
            }
        }else{
            $this->setError(lang('generic_error'));
        }
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
    public function delete($id)
    {

        $this->contact_model->delete(array('id' => $id));
        $this->setMsg(lang('deleted'));
        redirect($this->uri->segment(1) . '/' . $this->uri->segment(2));
    }
}