<div class="container-fluid home" id="home">
    <div class="category text-center">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2><?php echo lang('home_category_tlt');?></h2>
                <p><?php echo lang('home_category_p');?></p>
            </div>
            <div class="col-xs-8 col-xs-offset-2">
                <div class="row">
                    <a href="./institucional">
                        <img src="./assets/img/home/cat/off1.png" class="img-responsive off">
                        <img src="./assets/img/home/cat/on1.png" class="img-responsive on">
                    </a>
                    <a href="./carreira">
                        <img src="./assets/img/home/cat/off2.png" class="img-responsive off">
                        <img src="./assets/img/home/cat/on2.png" class="img-responsive on">
                    </a>
                    <a href="./institucional#ecology">
                        <img src="./assets/img/home/cat/off3.png" class="img-responsive off">
                        <img src="./assets/img/home/cat/on3.png" class="img-responsive on">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid testimonial">
    <div class="container">
        <div class="row text-center">
           <div class="col-md-8 col-md-offset-2">
               <h2><?php echo lang('testimonial_tlt');?></h2>
               <div id="Carousel" class="carousel slide">
                   <!-- Carousel items -->
                   <div class="carousel-inner">
                       <div class="item">
                           <div class="row">
                       <?php $i = 0; foreach($testimonial as $row): $i++; ?>
                           <?php if($i > 5): $i = 1; ?>
                           </div><!--.row-->
                       </div><!--.item-->
                       <div class="item">
                           <div class="row">
                           <?php endif;?>
                               <div class="col-xs-3 col-sm-3 col-md-2 item i<?php echo $row->id; ?>">
                                   <img  data-toggle="collapse" data-target="#collapsible-<?php echo $row->id; ?>" data-parent="#myAccordion" class="img-responsive" src="<?php echo $row->img; ?>" alt="" title="">
                               </div>
                       <?php endforeach; ?>
                           </div><!--.row-->
                       </div><!--.item-->
                   </div><!--.carousel-inner-->
                   <a data-slide="prev" href="#Carousel" class="left carousel-control">‹</a>
                   <a data-slide="next" href="#Carousel" class="right carousel-control">›</a>
               </div>
               <img  src="./assets/img/home/aspas.jpg" alt="">
               <div class="accordion" id="myAccordion">
                   <div class="panel">
                       <?php foreach($testimonial as $row): ?>
                       <div id="collapsible-<?php echo $row->id; ?>" class="collapse">
                           <p><?php echo $row->text; ?></p>
                           <h5><?php echo $row->identification; ?></h5>
                       </div>
                       <?php endforeach; ?>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>