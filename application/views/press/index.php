<div class="container-fluid press">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-1">
                    <h2><?php echo lang('press_tlt1');?></h2>
                    <h3><?php echo lang('press_sub_tlt');?></h3>
                </div>
            </div>
            <div class="gap"></div>
            <div class="row midia">
                <div class="col-md-10 col-md-offset-1">
                    <ul class="list-inline">
                        <?php foreach ($midia as $row):?>
                        <li>
                            <div class="box">
                                <?php if(isset($row->img) && strlen($row->img) > 0): ?>
                                <img class="img-responsive center-block" src="<?php echo $row->img; ?>" alt="<?php echo $row->title; ?>">
                                <?php endif ?>
                                <?php if(isset($row->title) && strlen($row->title) > 0): ?>
                                <h4><?php echo $row->title; ?></h4>
                                <?php endif ?>
                                <?php if(isset($row->subtitle) && strlen($row->subtitle) > 0): ?>
                                <p><?php echo $row->subtitle; ?></p>
                                <?php endif ?>
                                <?php if(isset($row->link) && strlen($row->link) > 0): ?>
                                <a class="more" href="<?php echo $row->link; ?>">Saiba Mais</a>
                                <?php endif ?>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="row releases">
                        <div class="col-md-12">
                            <h3><?php echo lang('press_sub_tlt1');?></h3>
                        </div>
                        <div class="gap"></div>
                        <?php foreach ($release as $row):?>
                        <div class="col-md-12">
                            <div class="box">
                                <?php if(isset($row->title) && strlen($row->title) > 0): ?>
                                <h1><?php echo $row->title; ?></h1>
                                <?php endif ?>
                                <?php if(isset($row->subtitle) && strlen($row->subtitle) > 0): ?>
                                <p><?php echo $row->subtitle; ?></p>
                                <?php endif ?>
                                <?php if(isset($row->link) && strlen($row->link) > 0): ?>
                                    <a class="more" href="<?php echo $row->link; ?>">Saiba Mais</a>
                                <?php endif ?>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container contact">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h4><?php echo lang('press_sub_tlt2');?></h4>
                    <ul class="list-group">
                    	<li class="list-group-item">Ideias & Efeito Assessoria de Comunicação</li>
                    	<li class="list-group-item">Camila Manchini – <span>camila@ienoticia.com.br</span></li>
                    	<li class="list-group-item">Rodrigo Pereira – <span> rodrigo@ienoticia.com.br</span></li>
                    	<li class="list-group-item">Audrei Franco – <span>audrei@ienoticia.com.br</span></li>
                    	<li class="list-group-item">Tel.: (11) 3277- 8252 / 3065-8253</li>
                    	<li class="list-group-item">www.ienoticia.com.br</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>