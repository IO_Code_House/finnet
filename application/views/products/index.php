<div class="container-fluid products">
    <div class="row border-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1">
                    <ul class="breadcrumb">
                        <li >
                            <?php echo lang('product_soluction');?>
                        </li>
                        <li>
                            <?php echo lang('products');?>
                        </li>
                        <li class="active">
                            <?php echo lang('financial_management');?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row financial">
                <div class="col-md-offset-1 col-md-7 ">
                    <h2><?php echo lang('products_tlt');?></h2>
                    <p><?php echo lang('products_p1');?></p>
                    <p><?php echo lang('products_p2');?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container list">
            <div class="row">
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h3><?php echo lang('products_list_tlt1');?></h3>
                        <p><?php echo lang('products_list_p1');?></p>
                        <a href="products#management" class="more"><?php echo lang('products_list_link');?></a>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h3><?php echo lang('refund_tlt');?></h3>
                        <p><?php echo lang('products_list_p2');?></p>
                        <a href="products#refund" class="more"><?php echo lang('products_list_link');?></a>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h3><?php echo lang('supplier_panel_nav');?></h3>
                        <p><?php echo lang('products_list_supplier_p');?></p>
                        <a href="products#supplier" class="more"><?php echo lang('products_list_link');?></a>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h3><?php echo lang('portal_slips_nav');?></h3>
                        <p><?php echo lang('products_list_portal_p');?></p>
                        <a href="products#slips" class="more"><?php echo lang('products_list_link');?></a>
                        <div class="gap"></div>
                        <p><?php echo lang('products_list_portal_p2');?></p>
                        <a href="./contato" class="more"><?php echo lang('contact');?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-financial">
    <div class="bg-submenu-fin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <div id="management"></div>
                        <ul class="list-inline nav-list">
                            <li class="active"><a href="products#management"><?php echo lang('management');?></a></li>
                            <li><a href="products#refund"><?php echo lang('refund');?></a></a></li>
                            <li><a href="products#supplier"><?php echo lang('supplier_panel_nav');?></a></li>
                            <li><a href="products#slips"><?php echo lang('portal_slips_nav');?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid management">
       <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-7">
                        <h2><?php echo lang('management_tlt');?></h2>
                        <p><?php echo lang('management_p1');?></p>
                        <div class="gap"></div>
                        <p><?php echo lang('management_p2');?></p>
                    </div>
                </div>
                <div class="row grid">
                    <div class="col-md-offset-1 col-md-4">
                        <h4><?php echo lang('management_pay_p1');?></h4>
                        <p><?php echo lang('management_pay_p2');?></p>
                        <div class="gap"></div>
                        <p><?php echo lang('management_pay_p3');?></p>
                        <div class="gap"></div>
                        <h4><?php echo lang('management_collection_p1');?></h4>
                        <p><?php echo lang('management_collection_p2');?></p>
                    </div>
                    <div class="col-md-offset-1 col-md-4">
                        <h4><?php echo lang('management_bar_code_p1');?></h4>
                        <p><?php echo lang('management_bar_code_p2');?></p>
                        <div class="gap"></div>
                        <h4><?php echo lang('management_extract_p1');?></h4>
                        <p><?php echo lang('management_extract_p2');?></p>
                    </div>
                </div>
            </div>
       </div>
    </div>
    <div class="bg-submenu-fin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <div id="refund"></div>
                        <ul class="list-inline nav-list">
                            <li><a href="products#management"><?php echo lang('management');?></a></li>
                            <li class="active"><a href="products#refund"><?php echo lang('refund');?></a></a></li>
                            <li><a href="products#supplier"><?php echo lang('supplier_panel_nav');?></a></li>
                            <li><a href="products#slips"><?php echo lang('portal_slips_nav');?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid refund">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                        <h2><?php echo lang('refund_tlt');?></h2>
                        <p><?php echo lang('refund_p1');?></p>
                        <p><?php echo lang('refund_p2');?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-submenu-fin">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div id="supplier"></div>
                    <ul class="list-inline nav-list">
                        <li><a href="products#management"><?php echo lang('management');?></a></li>
                        <li><a href="products#refund"><?php echo lang('refund');?></a></a></li>
                        <li class="active"><a href="products#supplier"><?php echo lang('supplier_panel_nav');?></a></li>
                        <li><a href="products#slips"><?php echo lang('portal_slips_nav');?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid supplier">
        <div class="row">
            <div class="container">
                <div class="row">
                        <div class="col-md-offset-1 col-md-6">
                        <h2><img src="./assets/img/products/supplier_icon.png" alt="icon supplier"><?php echo lang('supplier_panel');?></h2>
                            <div class="gap"></div>
                        <h2><?php echo lang('supplier_p1');?></h2>
                            <div class="gap"></div>
                        <p><?php echo lang('supplier_p2');?></p>
                            <div class="gap"></div>
                        <p><?php echo lang('supplier_p3');?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-submenu-fin">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div id="slips"></div>
                    <ul class="list-inline nav-list">
                        <li><a href="products#management"><?php echo lang('management');?></a></li>
                        <li><a href="products#refund"><?php echo lang('refund');?></a></a></li>
                        <li><a href="products#supplier"><?php echo lang('supplier_panel_nav');?></a></li>
                        <li class="active"><a href="products#slips"><?php echo lang('portal_slips_nav');?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid slips">
        <div class="row">
            <div class="container">
                <div class="row">
                        <div class="col-md-offset-1 col-md-6">
                        <h2><img src="./assets/img/products/slips_icon.png" alt="icon supplier"><?php echo lang('portal_slips_tlt');?></h2>
                        <h2><?php echo lang('portal_slips_p1');?></h2>
                        <p><?php echo lang('portal_slips_p2');?></p>
                            <div class="gap"></div>
                        <p><?php echo lang('portal_slips_p3');?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
