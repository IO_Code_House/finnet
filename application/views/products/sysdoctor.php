<div class="container">
    <div class="row">
        <div class="col-md-offset-1">
            <ul class="breadcrumb">
                <li >
                    <?php echo lang('product_soluction');?>
                </li>
                <li>
                    <?php echo lang('health');?>
                </li>
                <li class="active">
                    <?php echo lang('sysdoctor');?>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="container-main">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-7">
                <div class="gap"></div>
                <h3><?php echo lang('sysdoctor_p1');?></h3>
                <div class="gap"></div>
                <p><?php echo lang('sysdoctor_p2');?></p>
                <p><?php echo lang('sysdoctor_p3');?></p>
                <p><?php echo lang('sysdoctor_p4');?></p>
            </div>

            <div class="col-md-offset-1 col-md-7">
                <div class="gap"></div>
                <img src="./assets/img/products/sysdoctor.png" alt="" height="101" width="360" class="img-responsive" />
                <div class="gap"></div>
            </div>

            <div class="col-md-offset-1 col-md-7">
                <p><?php echo lang('sysdoctor_chang1');?></p>
                <p><?php echo lang('sysdoctor_p5');?></p>
                <p><?php echo lang('sysdoctor_p6');?></p>
                <p><?php echo lang('sysdoctor_p7');?></p>
                <p><?php echo lang('sysdoctor_p8');?></p>
                <a href="http://www.sysdoctor.com.br" target="_blank">www.sysdoctor.com.br</a>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
        </div>
    </div>
</div>