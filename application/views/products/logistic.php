<div class="row border-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1">
                <ul class="breadcrumb">
                    <li >
                        <?php echo lang('product_soluction');?>
                    </li>
                    <li>
                        <?php echo lang('products');?>
                    </li>
                    <li class="active">
                        <?php echo lang('logistic_management');?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid logistic-gde">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-8 ">
                    <div class="gap"></div>
                    <h2><?php echo lang('product_logistc_tlt');?></h2>
                    <div class="gap"></div>
                    <p><?php echo lang('product_logistc_p1');?></p>
                    <a href="./contato" class="more"><?php echo lang('contact');?></a>
                    <div class="gap"></div>
                    <img src="./assets/img/products/gde-web.png" alt="icon supplier">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container list-gde">
            <div class="row">
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h2><?php echo lang('product_gde_tlt');?></h2>
                        <div class="gap"></div>
                        <p><?php echo lang('product_gde_p1');?></p>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h5><?php echo lang('product_gde_tlt2');?></h5>
                        <p><?php echo lang('product_gde_p2');?></p>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h5><?php echo lang('product_gde_tlt3');?></h5>
                        <p><?php echo lang('product_gde_p3');?></p>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h5><?php echo lang('product_gde_tlt4');?></h5>
                        <p><?php echo lang('product_gde_p4');?></p>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-8 item">
                    <div class="box">
                        <h5><?php echo lang('product_gde_tlt5');?></h5>
                        <p><?php echo lang('product_gde_p5');?></p>
                        <div class="gap"></div>
                    </div>
                </div>
             </div>
        </div>
    </div>
</div>

