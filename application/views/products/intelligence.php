<div class="container-fluid intelligence">
    <div class="row border-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1">
                    <ul class="breadcrumb">
                        <li >
                            <?php echo lang('product_soluction');?>
                        </li>
                        <li>
                             <?php echo lang('products');?>
                        </li>
                        <li class="active">
                            <?php echo lang('intelligence_tlt1');?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row iten">
                <div class="col-md-offset-1 col-md-7">
                    <div class="gap"></div>
                    <div class="gap"></div>
                    <h3><?php echo lang('intelligence_tlt1_');?></h3>
                    <div class="gap"></div>
                    <p><?php echo lang('intelligence_p1');?></p>
                    <p><?php echo lang('intelligence_p2');?></p>
                    <p><?php echo lang('intelligence_p3');?></p>
                    <p><?php echo lang('intelligence_p4');?></p>
                    <a href="products#management" class="more"><?php echo lang('intellicence_link_1');?></a>

                </div>
            </div>
            <div class="row iten">
                <div class="col-md-offset-1 col-md-7 ">
                    <div class="gap"></div>
                    <h2><?php echo lang('intelligence_tlt2');?></h2>
                    <h3><?php echo lang('intelligence_sub_tlt2');?></h3>
                    <div class="gap"></div>
                    <p><?php echo lang('intelligence_p5');?></p>
                    <p><?php echo lang('intelligence_p6');?></p>
                    <p><?php echo lang('intelligence_p7');?></p>
                    <div class="gap"></div>
                    <div class="gap"></div>
                    <div class="gap"></div>
                </div>
            </div>
        </div>
    </div>
</div>
