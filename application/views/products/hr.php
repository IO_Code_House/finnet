<div class="breadcrumb-rack">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1">
                <ul class="breadcrumb">
                    <li >
                        <?php echo lang('product_soluction');?>
                    </li>
                    <li>
                        <?php echo lang('products');?>
                    </li>
                    <li class="active">
                        <?php echo lang('human_resource_management_department');?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-main">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-6">
                <h2><?php echo lang('hr_title1');?></h2>
                <div class="gap"></div>
            </div>
            <div class="col-md-offset-1 col-md-7">
                <p><?php echo lang('hr_p1');?></p>
                <div class="gap"></div>
                <p><a href=""><?php echo lang('hr_p3');?></a></p>
                <p><?php echo lang('hr_chang1');?></p>
                <p><a href="./contato"><?php echo lang('hr_chang2');?></a></p>
                <div class="gap"></div>
                <div class="gap"></div>
                <div class="gap"></div>
                <h4 ><?php echo lang('payroll');?></h4>
                <h3><?php echo lang('hr_title2');?></h3>
                <div class="gap"></div>
                <p><?php echo lang('hr_p5');?></p>
                <p><?php echo lang('hr_p6');?></p>
                <div class="gap"></div>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
        </div>
    </div>
</div>