<div class="service">
    <div class="bg-submenu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <div id="connectivity"></div>
                        <ul class="list-inline nav-list">
                            <li class="active"><a href="produtos/servicos#connectivity"><?php echo lang('connectivity');?></a></li>
                            <li><a href="produtos/servicos#software"><?php echo lang('software');?></a></a></li>
                            <li><a href="produtos/servicos#outsourcing"><?php echo lang('outsourcing');?></a></li>
                            <li><a href="produtos/servicos#bureau_swift"><?php echo lang('bureau_swift');?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row iten">
            <div class="col-md-offset-1 col-md-6">
                <div class="gap"></div>
                <h2><?php echo lang('connectivity');?></h2>
                <h3><?php echo lang('service_connectivity_subtlt');?></h3>
                <div class="gap"></div>
                <p><?php echo lang('service_connectivity_p1');?></p>
                <p><?php echo lang('service_connectivity_p2');?></p>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
        </div>
    </div>
    <div class="bg-submenu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <div id="software"></div>
                        <ul class="list-inline nav-list">
                            <li><a href="produtos/servico#connectivity"><?php echo lang('connectivity');?></a></li>
                            <li class="active"><a href="produtos/servicos#software"><?php echo lang('software');?></a></a></li>
                            <li><a href="produtos/servicos#outsourcing"><?php echo lang('outsourcing');?></a></li>
                            <li><a href="produtos/servicos#bureau_swift"><?php echo lang('bureau_swift');?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row iten">
            <div class="col-md-offset-1 col-md-6">
                <div class="gap"></div>
                <h2><?php echo lang('service_software_tlt');?></h2>
                <h3><?php echo lang('service_software_subtlt');?></h3>
                <div class="gap"></div>
                <p><?php echo lang('service_software_p1');?></p>
                <p><?php echo lang('service_software_p2');?></p>
                <p><?php echo lang('service_software_p3');?></p>
                <p><?php echo lang('service_software_p4');?></p>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
        </div>
    </div>
    <div class="bg-submenu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <div id="outsourcing"></div>
                        <ul class="list-inline nav-list">
                            <li><a href="produtos/servicos#connectivity"><?php echo lang('connectivity');?></a></li>
                            <li ><a href="produtos/servico#ssoftware"><?php echo lang('software');?></a></a></li>
                            <li class="active"><a href="produtos/servicos#outsourcing"><?php echo lang('outsourcing');?></a></li>
                            <li><a href="produtos/servicos#bureau_swift"><?php echo lang('bureau_swift');?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row iten">
            <div class="col-md-offset-1 col-md-6">
                <div class="gap"></div>
                <h2><?php echo lang('service_outsourcing_tlt_h2');?></h2>
                <h3><?php echo lang('service_outsourcing_subtlt');?></h3>
                <div class="gap"></div>
                <p><?php echo lang('service_outsourcing_p1');?></p>
                <p><?php echo lang('service_outsourcing_p2');?></p>
                <p><?php echo lang('service_outsourcing_p3');?></p>
                <p><?php echo lang('service_outsourcing_p4');?></p>
                <div class="gap"></div>
                <div class="gap"></div>

            </div>
        </div>
    </div>
    <div class="bg-submenu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <div id="bureau_swift"></div>
                        <ul class="list-inline nav-list">
                            <li><a href="produtos/servicos#connectivity"><?php echo lang('connectivity');?></a></li>
                            <li ><a href="produtos/servicos#software"><?php echo lang('software');?></a></a></li>
                            <li ><a href="produtos/servicos#outsourcing"><?php echo lang('outsourcing');?></a></li>
                            <li class="active"><a href="produtos/servicos#bureau_swift"><?php echo lang('bureau_swift');?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row iten">
            <div class="col-md-offset-1 col-md-6">
                <div class="gap"></div>
                <h2><?php echo lang('service_bureau_swift_tlt');?></h2>
                <h3><?php echo lang('service_bureau_swift_subtlt');?></h3>
                <div class="gap"></div>
                <p></p>
                <p><?php echo lang('service_bureau_swift_p1');?></p>
                <p><?php echo lang('service_bureau_swift_p2');?></p>
                <p><?php echo lang('service_bureau_swift_p3');?></p>
                <div class="gap"></div>
                <ul class="list">
                	<li class=""><?php echo lang('service_bureau_swift_i1');?></li>
                	<li class=""><?php echo lang('service_bureau_swift_i2');?></li>
                	<li class=""><?php echo lang('service_bureau_swift_i3');?></li>
                	<li class=""><?php echo lang('service_bureau_swift_i4');?></li>
                </ul>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
        </div>
    </div>
</div>
