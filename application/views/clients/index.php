<div class="container-fluid clients">
     <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-1  ">
                <h2><?php echo lang('clients_tlt');?></h2>
                <p><?php echo lang('clients_p1');?></p>
                <div class="gap"></div>
                <p><?php echo lang('clients_p2');?></p>
            </div>
        </div>
        <div class="row client-grid">
            <div class=" col-md-8 col-md-offset-1">
                <div class="row">
                    <?php foreach($client as $row):?>
                    <div class=" col-sm-6 col-md-4 col-lg-3 item">
                        <img data-toggle="tooltip" data-placement="bottom" class="img-responsive" src="<?php echo $row->img; ?>" alt="<?php echo $row->name; ?>" title="<?php echo $row->name; ?>">
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

