<div class="container">
    <table class="dataTable">
        <thead>
        <tr>
            <th><?php echo lang('company');?></th>
            <th><?php echo lang('name');?></th>
            <th><?php echo lang('email');?></th>
            <th><?php echo lang('zipcode');?></th>
            <th><?php echo lang('city');?></th>
            <th><?php echo lang('state');?></th>
            <th><?php echo lang('phone');?></th>
            <th><?php echo lang('reason');?></th>
            <th><?php echo lang('mesage');?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?php echo $row->company; ?></td>
                <td><?php echo $row->name; ?></td>
                <td><?php echo $row->email; ?></td>
                <td><?php echo $row->zipcode; ?></td>
                <td><?php echo $row->city; ?></td>
                <td><?php echo $row->state; ?></td>
                <td><?php echo $row->phone; ?></td>
                <td><?php echo $row->reason; ?></td>
                <td><?php echo $row->mesage; ?></td>
                <td class="action">
                    <a  href="./adm/<?php echo $this->uri->segment(2); ?>/excluir/<?php echo $row->id; ?>">
                        <i class="fa fa-trash-o fa-lg" title="<?php echo lang('remove');?>"></i>
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>