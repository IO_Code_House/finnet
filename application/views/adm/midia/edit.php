<div class="container">
    <h1><?php echo (isset($data->id) ? lang('edit') : lang('new')) . ' ' . lang('text') ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL ?>">
        <?php foreach($avaliable_lang as $row):?>
            <div class="form-group">
                <label for="title<?php echo $row; ?>"><?php echo lang('title'); ?> - <?php echo $row; ?>: </label>
                <input class="form-control" type="text" name="title[<?php echo $row; ?>]" id="name<?php echo $row; ?>" value="<?php echo isset($data->title[$row]) ? $data->title[$row] : NULL ?>" />
            </div>
            <div class="form-group">
                <label for="subtitle<?php echo $row; ?>"><?php echo lang('subtitle'); ?> - <?php echo $row; ?>: </label>
                <input class="form-control" type="text" name="subtitle[<?php echo $row; ?>]" id="name<?php echo $row; ?>" value="<?php echo isset($data->subtitle[$row]) ? $data->subtitle[$row] : NULL ?>" />
            </div>
        <?php endforeach;?>
        <div class="form-group">
            <label for="link">Link: </label>
            <input class="form-control" type="text" name="link" id="link" value="<?php echo isset($data->link) ? $data->link : NULL ?>" />
        </div>
        <div class="form-group">
            <label for="img"><?php echo lang('img');?>: </label>
            <input type="file" name="img" id="img" class="form-control" />
        </div>
        <?php if(isset($data->img)) : ?>
            <div class="form-group">
                <label for="img">
                    <img src="<?php echo $data->img; ?>" class="img-thumbnail" alt="" />
                </label>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="<?php echo lang('save');?>">
        </div>
    </form>
</div>
