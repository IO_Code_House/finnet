<div class="container">
    <table class="dataTable">
        <thead>
        <tr>
            <th><?php echo lang('name');?></th>
            <th class="image"><?php echo lang('img');?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($list as $row) :?>
            <tr>
                <td><?php echo $row->name; ?></td>
                <td><img src="<?php echo $row->img; ?>" alt="" class="img-thumbnail"></td>
                <td class="action">
                    <a href="./adm/<?php echo $this->uri->segment(2); ?>/editar/<?php echo $row->id; ?>">
                        <i class="fa fa-pencil" title="<?php echo lang('edit');?>"></i>
                    </a>
                    <a  href="./adm/<?php echo $this->uri->segment(2); ?>/excluir/<?php echo $row->id; ?>">
                        <i class="fa fa-trash-o fa-lg" title="<?php echo lang('remove');?>"></i>
                    </a>
                </td>
            </tr>
        <?php endForeach;?>
        </tbody>
    </table>
</div>