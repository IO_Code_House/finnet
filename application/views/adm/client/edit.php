<div class="container">
    <h1><?php echo (isset($data->id) ? lang('edit') : lang('new')) . ' ' . lang('client') ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL ?>">
        <div class="form-group">
            <label for="name"><?php echo lang('name');?>: </label>
            <input class="form-control" type="text" name="name" id="name" value="<?php echo isset($data->name) ? $data->name : NULL ?>" required="" placeholder="Insira aqui o título para o post" />
        </div>
        <div class="form-group">
            <label for="img"><?php echo lang('img');?>: </label>
            <input type="file" name="img" id="img" class="form-control" />
        </div>
        <?php if(isset($data->img)) : ?>
            <div class="form-group">
                <label for="img">
                    <img src="<?php echo $data->img; ?>" class="img-thumbnail" alt="" />
                </label>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="<?php echo lang('save');?>">
        </div>
    </form>
</div>
