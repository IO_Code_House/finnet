<div class="container">
    <h1><?php echo (isset($data->id) ? lang('edit') : lang('new')) . ' ' . lang('testimonial') ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL ?>">
        <?php foreach($avaliable_lang as $row):?>
            <div class="form-group">
                <label for="identification<?php echo $row; ?>"><?php echo lang('identification'); ?> - <?php echo $row; ?>: </label>
                <input class="form-control" type="text" name="identification[<?php echo $row; ?>]" id="name<?php echo $row; ?>" value="<?php echo isset($data->identification[$row]) ? $data->identification[$row] : NULL ?>" />
            </div>

            <div class="form-group">
                <label for="text<?php echo $row; ?>"><?php echo lang('text'); ?> - <?php echo $row; ?>: </label>
                <input class="form-control" type="text" name="text[<?php echo $row; ?>]" id="name<?php echo $row; ?>" value="<?php echo isset($data->text[$row]) ? $data->text[$row] : NULL ?>" />
            </div>
        <?php endforeach;?>
        <div class="form-group">
            <label for="img"><?php echo lang('img');?>: </label>
            <input type="file" name="img" id="img" class="form-control" />
        </div>
        <?php if(isset($data->img)) : ?>
            <div class="form-group">
                <label for="img">
                    <img src="<?php echo $data->img; ?>" class="img-thumbnail" alt="" />
                </label>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="<?php echo lang('save');?>">
        </div>
    </form>
</div>
