<div class="container">
    <h1><?php echo (isset($data->id) ? lang('edit') : lang('new')) . ' ' . lang('admin') ?> </h1>
    <form role="form" method="post" enctype="multipart/form-data" action="./<?php echo $this->uri->segment(1); ?>/<?php echo $this->uri->segment(2); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL ?>">
        <div class="form-group">
            <label for="name"><?php echo lang('name');?>: </label>
            <input class="form-control" type="text" name="name" id="name" value="<?php echo isset($data->name) ? $data->name : NULL ?>"/>
        </div>
        <div class="form-group">
            <label for="email"><?php echo lang('email');?>: </label>
            <input class="form-control" type="email" name="email" id="email" value="<?php echo isset($data->email) ? $data->email : NULL ?>"/>
        </div>
        <div class="form-group">
            <label for="password"><?php echo lang('password');?>: </label>
            <input class="form-control" type="password" name="password" id="password" value="" placeholder=""/>
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="<?php echo lang('save');?>">
        </div>
    </form>
</div>
