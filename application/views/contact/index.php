<div class="conteiner-fluid contact">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-1 text">
                <p><?php echo lang('contact_p1');?></p>
                <div class="gap"></div>
                <p><?php echo lang('contact_p2');?></p>
                <div class="gap"></div>
                <p><?php echo lang('contact_p3');?></p>
                <div class="gap"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <form action="./contato/enviar" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="company"><?php echo lang('company');?></label>
                                <input id="company" name="company" type="text" class="form-control" required="required" aria-required="true">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name"><?php echo lang('name');?></label>
                                <input id="name" name="name" type="text" class="form-control" required="required" aria-required="true">
                            </div>
                        </div>
                           <div class="col-md-12">
                            <div class="form-group">
                                <label for="email"><?php echo lang('email');?></label>
                                <input name="email" id="email" type="email" class="form-control" required="required" aria-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="zipcode"><?php echo lang('zipcode');?></label>
                                <input id="zipcode" name="zipcode" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city"><?php echo lang('city');?></label>
                                <input id="city" name="city" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="state"><?php echo lang('state');?></label>
                                <input id="state" name="state" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone"><?php echo lang('phone');?></label>
                                <input id="phone" name="phone" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="reason"><?php echo lang('reason');?></label>
                                <select class="form-control reason" id="reason" name="reason">
                                    <option><?php echo lang('commercial');?></option>
                                    <option><?php echo lang('tech_suport');?></option>
                                    <option><?php echo lang('thanks_ask_other');?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label  for="mesage"><?php echo lang('mesage');?></label>
                                <textarea id="mesage" name="mesage" class="form-control" rows="5"></textarea>
                            </div>
                            <button class="btn btn-primary pull-left submit" type="submit"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12">
                        <div class=" support">
                            <h6><?php echo lang('phone_support');?></h6>
                            <ul>
                                <li>
                                    <p><?php echo lang('support_commercial');?></p>
                                    <p><?php echo lang('support_commercial_p1');?></p>
                                    <p><?php echo lang('phone');?> 11 3882-9222</p>
                                </li>
                                <li>
                                    <p><?php echo lang('support_support');?></p>
                                    <p><?php echo lang('support_support_p1');?></p>
                                    <p><?php echo lang('phone');?> 11 4133-4455</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="support chat">
                            <h6><?php echo lang('chat_support');?></h6>
                            <p>
                                <!-- LiveZilla Tracking Code (ALWAYS PLACE IN BODY ELEMENT) --><div id="livezilla_tracking" style="display:none"></div><script type="text/javascript">
                                var script = document.createElement("script");script.async=true;script.type="text/javascript";var src = "http://atendimento.finnet.com.br/server.php?a=f8a9f&rqst=track&output=jcrpt&hg=P1N1cG9ydGU,U3Vwb3J0ZSAtIENvbnNpZ25hZG8gU2FudGFuZGVy&nse="+Math.random();setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)",1);</script><noscript><img src="http://atendimento.finnet.com.br/server.php?a=f8a9f&amp;rqst=track&amp;output=nojcrpt&amp;hg=P1N1cG9ydGU,U3Vwb3J0ZSAtIENvbnNpZ25hZG8gU2FudGFuZGVy" width="0" height="0" style="visibility:hidden;" alt=""></noscript><!--http://www.LiveZilla.net Tracking Code --><!-- LiveZilla Text Chat Link Code (ALWAYS PLACE IN BODY ELEMENT) --><script type="text/javascript" id="lz_textlink" src="http://atendimento.finnet.com.br/image.php?a=370b8&amp;tl=1&amp;srv=aHR0cDovL2F0ZW5kaW1lbnRvLmZpbm5ldC5jb20uYnIvY2hhdC5waHA,YT0wNzU5NSZoZz1QMU4xY0c5eWRHVSxVM1Z3YjNKMFpTQXRJRU52Ym5OcFoyNWhaRzhnVTJGdWRHRnVaR1Z5&amp;tlont=QWp1ZGEgKE9ubGluZSk_&amp;tloft=QWp1ZGEgLSBEZWl4ZSB1bWEgbWVuc2FnZW0gKE9mZmxpbmUp&amp;hg=P1N1cG9ydGU,U3Vwb3J0ZSAtIENvbnNpZ25hZG8gU2FudGFuZGVy"></script><!-- http://www.LiveZilla.net Text Chat Link Code --></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>