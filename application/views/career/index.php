<div class="container">
    <div class="row">
    <div class="col-md-12">
        <div class="text-center">
            <div id="carreira"></div>
            <ul class="list-inline nav-list">
                <li class="active"><a href="carreira#career"><?php echo lang('career');?></a></li>
                <li><a href="carreira#development"><?php echo lang('development');?></a></a></li>
                <li><a href="carreira#culture"><?php echo lang('culture');?></a></li>
                <li><a href="carreira#curriculum"><?php echo lang('curriculum');?></a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
<div class="container-career">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-5">
                    <h2><?php echo lang('career_tlt');?></h2>
                    <p><?php echo lang('career_p1');?></p>
                    <div class="gap"></div>
                    <p><?php echo lang('career_p2');?></p>
                    <div class="gap"></div>
                    <p><?php echo lang('career_p3');?></p>
                    <div class="gap"></div>
                    <p><?php echo lang('career_p4');?></p>
                </div>
            </div>
        </div>
</div>
<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="text-center">
               <div id="development"></div>
               <ul class="list-inline nav-list">
                   <li><a href="carreira#career"><?php echo lang('career');?></a></li>
                   <li class="active"><a href="carreira#development"><?php echo lang('development');?></a></a></li>
                   <li><a href="carreira#culture"><?php echo lang('culture');?></a></li>
                   <li><a href="carreira#curriculum"><?php echo lang('curriculum');?></a></li>
               </ul>
           </div>
       </div>
    </div>
</div>
<div class="container-development">
        <div class="container tabs">
            <div class="row">
                <div class="col-md-5 col-md-offset-1">
                    <h2><?php echo lang('development');?></h2>
                    <p><?php echo lang('development_p1');?></p>
                    <div class="gap"></div>
                    <p><?php echo lang('development_p2');?></p>
                    <div class="gap"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-md-offset-1 tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li><a href="javascript:;" data-html="true" data-placement="top" data-toggle="popover" data-content="<p><?php echo lang('development_tab1_txt1');?></p><p><?php echo lang('development_tab1_txt2');?></p>"><?php echo lang('development_tab1');?></a></li>
                        <li><a href="javascript:;" data-html="true" data-placement="top" data-toggle="popover" data-content="<p><?php echo lang('development_tab2_txt1');?></p>"><?php echo lang('development_tab2');?></a></li>
                        <li><a href="javascript:;" data-html="true" data-placement="top" data-toggle="popover" data-content="<p><?php echo lang('development_tab3_txt1');?></p><p><?php echo lang('development_tab3_txt2');?></p>"><?php echo lang('development_tab3');?></a></li>
                        <li><a href="javascript:;" data-html="true" data-placement="top" data-toggle="popover" data-content="<p><?php echo lang('development_tab4_txt');?></p>"><?php echo lang('development_tab4');?></a></li>
                    </ul>
                </div>
            </div>
        </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <div id="culture"></div>
                <ul class="list-inline nav-list">
                    <li><a href="carreira#career"><?php echo lang('career');?></a></li>
                    <li><a href="carreira#development"><?php echo lang('development');?></a></a></li>
                    <li class="active"><a href="carreira#culture"><?php echo lang('culture');?></a></li>
                    <li><a href="carreira#curriculum"><?php echo lang('curriculum');?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-culture">
     <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-2">
                    <h4><?php echo lang('culture');?></h4>
                    <h2><?php echo lang('culture_tlt');?></h2>
                    <p><?php echo lang('culture_p1');?></p>
                    <div class="gap"></div>
                    <p><?php echo lang('culture_p2');?></p>
                    <div class="gap"></div>
                    <p><?php echo lang('culture_p3');?></p>
                </div>
            </div>
        </div>
    </div>
<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="text-center">
               <div id="curriculum"></div>
               <ul class="list-inline nav-list">
                   <li><a href="carreira#career"><?php echo lang('career');?></a></li>
                   <li><a href="carreira#development"><?php echo lang('development');?></a></a></li>
                   <li><a href="carreira#culture"><?php echo lang('culture');?></a></li>
                   <li class="active"><a href="carreira#curriculum"><?php echo lang('curriculum');?></a></li>
               </ul>
           </div>
       </div>
    </div>
</div>
<div class="container-curriculum">
     <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-3 text-center">
                    <h2><?php echo lang('curriculum_tlt');?></h2>
                    <h3><?php echo lang('curriculum_p1');?></h3>
                    <p><?php echo lang('curriculum_p2');?></p>
                    <div class="gap"></div>
                    <a target="_blank" href="https://site.vagas.com.br/IdCandColetaCur.asp" class="more"><?php echo lang('curriculum_link');?></a>
                    <div class="col-md-12 socials">
                        <a href="https://site.vagas.com.br/VagasDe1Empr.asp?t=521" target="_blank">
                            <img src="./assets/img/career/icon_vagas.png" alt="">
                        </a>
                    </div>
                </div>
           </div>
     </div>
</div>
