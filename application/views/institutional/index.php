<div class="container ">
    <div class="row">
       <div class="col-md-12 text-center">
           <div id="who-we"></div>
           <ul class="list-inline nav-list">
               <li class="active"><a href="institucional#who-we"><?php echo lang('who_we');?></a></li>
               <li><a href="institucional#fortress"><?php echo lang('our_strengths');?></a></a></li>
               <li><a href="institucional#ecology"><?php echo lang('ecology');?></a></li>
               <li><a href="institucional#units"><?php echo lang('units');?></a></li>
           </ul>
       </div>
    </div>
</div>
<div class="container-fluid who-we">
    <div class="row">
        <div id="who-we" class="container ">
            <div class="row">
                <div class="col-md-offset-1 col-md-5 ">
                    <h2><?php echo lang('who_she_tlt');?></h2>
                    <p><?php echo lang('who_she_p1');?></p>
                    <p><?php echo lang('who_she_p2');?></p>
                    <p><?php echo lang('who_she_p3');?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container ">
    <div class="row">
       <div class="col-md-12 text-center">
           <div id="fortress"></div>
           <ul class="list-inline nav-list">
               <li ><a href="institucional#who-we"><?php echo lang('who_we');?></a></li>
               <li class="active"><a href="institucional#fortress"><?php echo lang('our_strengths');?></a></a></li>
               <li><a href="institucional#ecology"><?php echo lang('ecology');?></a></li>
               <li><a href="institucional#units"><?php echo lang('units');?></a></li>
               <gap></gap>
           </ul>
       </div>
    </div>
</div>
<div class="container-fluid fortress">
    <div class="row">
        <div id="fortress" class="container ">
            <div class="row">
                    <div class="col-md-10 col-md-offset-1  text-center">
                        <h2><?php echo lang('our_strengths_tlt');?></h2>
                        <p><?php echo lang('our_strengths_p1');?> </p>
                        <p><?php echo lang('our_strengths_p2');?></p>
                        <div class="gap"></div>
                        <a href="./"><?php echo lang('our_strengths_cta');?> </a>
                        <p><?php echo lang('our_strengths_p3');?></p>
                        <div class="gap"></div>
                        <div class="gap"></div>
                    </div>
            </div>
        </div>
 </div>
</div>
<div class="container ">
    <div class="row">
        <div class="col-md-12 text-center">
            <ul class="list-inline nav-list">
                <div id="ecology"></div>
                <li ><a href="institucional#who-we"><?php echo lang('who_we');?></a></li>
                <li><a href="institucional#fortress"><?php echo lang('our_strengths');?></a></a></li>
                <li class="active"><a href="institucional#ecology"><?php echo lang('ecology');?></a></li>
                <li><a href="institucional#units"><?php echo lang('units');?></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid ecology">
    <div class="row">
        <div id="ecology" class="container ">
            <div class="row">
                <div class="col-md-10 col-md-offset-1  text-center">
                    <h2><?php echo lang('ecology_tlt');?></h2>
                    <p><?php echo lang('ecology_p1');?></p>
                    <p><?php echo lang('ecology_p2');?></p>
                    <p><?php echo lang('ecology_p3');?></p>
                    <div class="gap"></div>
                    <a href="./"><?php echo lang('ecology_cta');?></a>
                    <div class="gap"></div>
                    <div class="gap"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container ">
    <div class="row">
        <div class="col-md-12 text-center">
            <div id="units"></div>
            <ul class="list-inline nav-list">
                <li><a href="institucional#who-we"><?php echo lang('who_we');?></a></li>
                <li><a href="institucional#fortress"><?php echo lang('our_strengths');?></a></a></li>
                <li><a href="institucional#ecology"><?php echo lang('ecology');?></a></li>
                <li class="active"><a href="institucional#units"><?php echo lang('units');?></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid units">
    <div class="row">
        <div id="units" class="container ">
            <div class="row">
                <div class="col-md-8 col-md-offset-2  text-center">
                    <h2><?php echo lang('units_tlt');?></h2>
                    <p><?php echo lang('units_p1');?></p>
                    <p class="p-responsive"><?php echo lang('units_p2');?></p>
                    <div class="gap"></div>
                </div>
            </div>
            <div class="row pin-units">
                <div class="col-md-12">
                    <a class="popoverr pin-eua" role="button" data-container="body" data-toggle="popover"  data-placement="right" data-placement="bottom"
                       data-content="<p>MELBOURNE</p>
                    <?php echo lang('melbourne_text');?>
               <br><br>
               <?php echo lang('localization');?>: <br>
               1135 NASA BLV - MELBOURNE - FL<br>
               <?php echo lang('phone');?>: +1 (321) 802-6900"
                       data-html="true"><i class="fa fa-map-marker"></i></a>
                    <a class="popoverr pin-sp" role="button" data-container="body" data-toggle="popover"  data-placement="right" data-placement="bottom"
                       data-content="<p>SÃO PAULO - SP</p>
                  <?php echo lang('sao_paulo_text');?>
               <br><br>
               <?php echo lang('localization');?>: <br>
               Rua Itapeva 286 – 14º andar <br>
               Bela Vista - São Paulo - SP – Brasil<br>
               <?php echo lang('zipcode');?>: 01332-000<br>
               <?php echo lang('phone');?>: 55 11 3882.9200<br>"
                       data-html="true"><i class="fa fa-map-marker"></i></a>
                    <a class="popoverr pin-1" role="button" data-container="body" data-toggle="popover"  data-placement="right" data-placement="bottom"
                       data-content="<p>BARUERI - SP</p>
                       <?php echo lang('barueri_text');?>
               <br><br>
               <?php echo lang('localization');?>: <br>
               Alameda: Araguaia, 2044 – 3°and.- Alphaville - Barueri - SP – Brasil <br>
               <?php echo lang('zipcode');?>: 06455-000<br>
               <?php echo lang('phone');?>: 55 11 4133.4455<br>"
                       data-html="true"><i class="fa fa-map-marker"></i></a>
                    <a class="popoverr pin-2" role="button" data-container="body" data-toggle="popover"  data-placement="right" data-placement="bottom"
                       data-content="<p>RIBEIRÃO PRETO - SP</p>
               <?php echo lang('ribeirao_preto_text');?>: <br>
              <br><br>
               <?php echo lang('localization');?>: <br>
               R. Sete de Setembro, 590, sala 51-B - Centro <br>
               <?php echo lang('zipcode');?>: 14010-180<br>
               <?php echo lang('phone');?>: 55 16 3977-6667<br>"
                       data-html="true"><i class="fa fa-map-marker"></i></a>
                    <a class="popoverr pin-3" role="button" data-container="body" data-toggle="popover"  data-placement="right" data-placement="bottom"
                       data-content="<p>SÃO JOSE DOS CAMPOS - SP</p>
               <?php echo lang('sje_text');?>: 14010-180<br>
               <br><br>
               <?php echo lang('localization');?>: <br>
               Avenida Doutor João Guilhermino, Nº 261 – 11º Andar  – Centro – São José dos Campos.<br>
               <?php echo lang('zipcode');?>: 12210-131<br>
               <?php echo lang('phone');?>: 55 12 3911-3981<br>"
                       data-html="true"><i class="fa fa-map-marker"></i></a>
                    <a class="popoverr pin-rs" role="button" data-container="body" data-toggle="popover"  data-placement="right" data-placement="bottom"
                       data-content="<p>RIO GRANDE DO SUL</p>
                 <?php echo lang('rio_grande_text');?>: <br>
               <br><br>
               <?php echo lang('localization');?>: <br>
              Av Carlos Gomes, 222 - sala 833 - Bairro Auxiliadora - Porto Alegre – RS<br>
               <?php echo lang('zipcode');?>: 90480-000<br>
               <?php echo lang('phone');?>: 55 51 3378-1054<br>"
                       data-html="true"><i class="fa fa-map-marker"></i></a>

                    <img src="./assets/img/institutional/mapa.jpg"/>
                </div>
            </div>

        </div>
       </div>
</div>

