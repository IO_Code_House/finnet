<div class="container-fluid footer">
    <div class="row">
      <div class="container">
        <div class="row">
            <div class="gap"></div>
            <div class="local">
                <div class="col-md-12 text-center">
                    <p><img src="./assets/img/local.png" alt="icon localiztion"><?php echo lang('local_contact');?></p>
                </div>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
            <div class="col-md-3 address">
                <p>ALPHAVILLE </p>
                <p>
                    Al. Araguaia, 2044 – 3°and.<br>
                    Alphaville - Barueri - SP - Brasil<br>
                    CEP: 06455-000<br><br>

                </p>
            </div>
            <div class="col-md-3 address">
                <p>PAULISTA</P>
                <P>
                    Rua Itapeva 286 – 14º andar<br>
                    Bela Vista - São Paulo - SP - Brasil<br>
                    CEP: 01332-000 <br><br>

                </p>
            </div>
            <div class="col-md-3 text-center">
                <ul class="list-inline">
                    <li>
                        <a href="https://www.youtube.com/user/FinnetBrasil" target="_blank">
                            <img src="./assets/img/tube.png" alt="Youtube">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/finnet-comercio-e-servi-os-de-teleinform-tica-ltda-?trk=job_view_topcard_company_name" target="_blank">
                            <img src="./assets/img/in.png" alt="In">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/FinnetBrasil/?ref=hl" target="_blank">
                            <img src="./assets/img/face.png" alt="Facebook">
                        </a>
                    </li>
                </ul>
                <button onclick="window.location.href='./contato/';" type="button" class="btn"><?php echo lang('contact_us');?></a></button><br>
                <button onclick="window.location.href='./institucional#units';"type="button" class="btn"><?php echo lang('our_branches');?></a></button>
                <div class="gap"></div>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="container-fluid footer-1">
    <div class="row">
      <div class="container">
          <div class="gap"></div>
          <div class="col-md-4">
              <p><?php echo lang('rights');?></p>
          </div>
          <div class="col-md-8">
              <ul class="nav nav-pills nav-footer">
                      <li class="institutional"><a href="./institutional"><?php echo lang('institutional');?></a></li>
                  <li class="products dropup">
                      <a href="#" id="product_soluction" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('product_soluction');?> <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="product_soluction">
                          <li><a href="./produtos/financeira"><?php echo lang('financial_management');?></a></li>
                          <li><a href="./produtos/logistica"><?php echo lang('logistic_management');?></a></li>
                          <li><a href="./produtos/inteligencia"><?php echo lang('intelligence');?></a></li>
                          <li><a href="./produtos/sysdoctor"><?php echo lang('sysdoctor');?></a></li>
                          <li><a href="./produtos/rh"><?php echo lang('human_resource_management_department');?></a></li>
                          <li><a href="./produtos/servicos"><?php echo lang('service');?></a></li>
                      </ul>
                  </li>
                      <li class="clients"><a href="./clientes"><?php echo lang('clients');?></a></li>
                      <li class="press"><a href="./imprensa"><?php echo lang('press');?></a></li>
                      <li class="career"><a href="./carreira"><?php echo lang('career');?></a></li>
                      <li class="contact"><a href="./contato"><?php echo lang('contact');?></a></li>
               </ul>
          </div>
      </div>
        <div class="gap"></div>
    </div>
</div>
</body>
</html>