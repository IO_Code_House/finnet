<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <base href="<?php echo base_url(); ?>">
    <meta name="controller" content="<?php echo $this->router->class ?>"/>
    <meta name="method" content="<?php echo $this->router->method ?>"/>
<?php echo isset($assets) ? $assets : NULL; ?>
<?php echo isset($css) ? $css : NULL; ?>
<?php echo isset($js) ? $js : NULL; ?>
</head>
<body class="<?php echo $this->router->class ?>_<?php echo $this->router->method ?>">

<?php if(isset($hive)): ?>
<div class="hive">
    <img src="<?php echo $hive;?>" class="img-responsive">
</div>
<?php endif; ?>


<div class="global-inf">
    <?php if($error):?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Erro:</strong> <?php echo $error; ?>
        </div>
    <?php endif;?>
    <?php if($msg):?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Sucesso:</strong> <?php echo $msg; ?>
        </div>
    <?php endif;?>
</div>
<div class="header">
    <div class="container-fluid">
        <div class="container">
            <nav class="col-md-offset-1 navbar navbar-default " role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle toggle-header" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="./">
                        <img src="./assets/img/logo.png" alt="">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse col-md-offset-1">
                    <ul class="nav navbar-nav nav-header">
                        <li class="institutional"><a href="./institucional"><?php echo lang('institutional');?></a></li>
                        <li class="products">
                            <a href="#" id="product_soluction" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('product_soluction');?> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="product_soluction">
                                <li><a href="./produtos/financeira"><?php echo lang('financial_management');?></a></li>
                                <li><a href="./produtos/logistica"><?php echo lang('logistic_management');?></a></li>
                                <li><a href="./produtos/inteligencia"><?php echo lang('intelligence');?></a></li>
                                <li><a href="./produtos/sysdoctor"><?php echo lang('sysdoctor');?></a></li>
                                <li><a href="./produtos/rh"><?php echo lang('human_resource_management_department');?></a></li>
                                <li><a href="./produtos/servicos"><?php echo lang('service');?></a></li>
                            </ul>
                        </li>
                        <li class="clients"><a href="./clientes"><?php echo lang('clients');?></a></li>
                        <li class="press"><a href="./imprensa"><?php echo lang('press');?></a></li>
                        <li class="career"><a href="./carreira"><?php echo lang('career');?></a></li>
                        <li class="contact"><a href="./contato"><?php echo lang('contact');?></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right language">
                        <li><a href="?lang=portuguese"><img src="./assets/img/lang/portuguese.jpg" alt=""></a></li>
                        <li><a href="?lang=english"><img src="./assets/img/lang/english.jpg" alt=""></a></li>
                        <li><a href="?lang=spanish"><img src="./assets/img/lang/spanish.jpg" alt=""></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>
</div>

<div id="carousel_banner" class="carousel slide banner" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php $banner = (is_array($banner) ? $banner : $banner); $i = 0; foreach ($banner as $row): ?>
            <li data-target="#carousel_banner" data-slide-to="<?php echo $i++; ?>"></li>
        <?php endforeach; ?>
    </ol>
     <div class="carousel-inner " role="listbox">
        <?php foreach ($banner as $row): ?>
            <div class="item">
                <img  class="" src="<?php echo $row->img; ?>" alt="">
                <div class="container title">
                    <?php if(isset($row->title)): ?>
                        <hr />
                        <h1 class="animate"><?php echo $row->title; ?></h1>
                        <hr />
                    <?php endif; ?>
                    <?php if(isset($row->subtitle)): ?>
                        <h2 class="animate"><?php echo $row->subtitle; ?></h2>
                    <?php endif; ?>
                    <?php if(isset($row->link)): ?>
                        <button onclick="window.location.href='<?php echo $row->link; ?>';" type="button" class="btn btn-home"><?php echo lang('more');?></a></button><br>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
     </div>
    <a class="left carousel-control" href="#carousel_banner" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel_banner" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>