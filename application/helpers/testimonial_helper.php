<?php
function parse_testimonial(&$list, $lang)
{
    if (!is_array($list)) {
        $list = (array)$list;
    }
    foreach ($list as $key => $row) {
        if (isset($row->text)) {
            $row->text = unserialize($row->text);
            $row->text = isset($row->text[$lang]) ? $row->text[$lang] : null;
        }
        if (isset($row->identification)) {
            $row->identification = unserialize($row->identification);
            $row->identification = isset($row->identification[$lang]) ? $row->identification[$lang] : null;
        }
        $list[$key] = $row;
    }
}