<?php
function parse_release(&$list, $lang)
{
    if (!is_array($list)) {
        $list = (array)$list;
    }
    foreach ($list as $key => $row) {
        if (isset($row->title)) {
            $row->title = unserialize($row->title);
            $row->title = isset($row->title[$lang]) ? $row->title[$lang] : null;
        }
        if (isset($row->subtitle)) {
            $row->subtitle = unserialize($row->subtitle);
            $row->subtitle = isset($row->subtitle[$lang]) ? $row->subtitle[$lang] : null;
        }
        $list[$key] = $row;
    }
}