<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

/**
 * Class MY_Controller
 */
class  MY_Controller extends CI_Controller
{

    /**
     * @var array
     */
    public $data = array();
    /**
     * @var string
     */
    public $content = '';
    /**
     * @var string
     */
    public $header = 'default/header';
    /**
     * @var string
     */
    public $footer = 'default/footer';
    /**
     * @var string
     */
    protected $assets = array();
    /**
     * @var string
     */
    protected $css = array();
    /**
     * @var string
     */
    protected $js = array();

    /**
     *
     */
    public function debug() {
        print_r($this);
    }

    public function __construct()
    {
        parent::__construct();

        $this->set_lang();

        $this->loadCss(array(
            array(
                'name' => 'bootstrap'
            ),
            array(
                'name' => 'font-awesome'
            ),
            array(
                'name' => 'bootstrap-theme'
            )
        ), true);

        $this->loadJs(array(
            array(
                'name' => 'jquery-1.11.3'
            ),
            array(
                'name' => 'bootstrap'
            )
        ), true);

        $this->data['me'] = $this->session->userdata('me')?$this->session->userdata('me'):null;
        $this->data['adm'] = $this->session->userdata('adm')?$this->session->userdata('adm'):null;
        if ($this->uri->segment(1) == 'adm') {
            if ($this->router->class != 'login' && isset($this->data['adm']->id) === FALSE) {
                $this->setError('É necessário estar logado');
                $this->setPreviousUrl(base_url($this->uri->uri_string()));
                redirect(base_url('/adm/login'), 'refresh');
            }
            if ($this->header !== NULL) {
                $this->header = 'adm/' . $this->header;
            }
            if ($this->footer !== NULL) {
                $this->footer = 'adm/' . $this->footer;
            }
            $this->content = file_exists(APPPATH . 'views/' . $this->uri->segment(1) . '/' . $this->router->class . '/' . $this->router->method . '.php') ? $this->uri->segment(1) . '/' . $this->router->class . '/' . $this->router->method : 'default/content';


            //load ui
            $this->loadCss(array(
                array(
                    'name' => 'jquery-ui',
                    'path' => 'assets/jquery-ui/css/smoothness/'
                ),
                array(
                    'name' => 'jquery.ui.theme',
                    'path' => 'assets/jquery-ui/css/smoothness/'
                ),
            ), true);

            $this->loadJs(array(
                array(
                    'name' => 'jquery-ui.min',
                    'path' => 'assets/jquery-ui/js/'
                ),
                array(
                    'name' => 'jquery.ui.datepicker.regional.pt',
                    'path' => 'assets/jquery-ui/js/'
                )
            ), true);

            $this->avaliable_lang();

        }elseif ($this->uri->segment(1) == 'ws') {

        }else {
            $this->loadCss(array('name' => 'normalize'));
            $this->content = file_exists(APPPATH . 'views/' . $this->router->class . '/' . $this->router->method . '.php') ? $this->router->class . '/' . $this->router->method : 'default/content';
        }

        $this->data['error'] = $this->session->flashdata('error') ? $this->session->flashdata('error') : null;
        $this->data['msg'] = $this->session->flashdata('msg') ? $this->session->flashdata('msg') : null;



    }

    /**
     * Call the view
     *
     * @access  public
     */
    public function index()
    {
        $this->renderer();
    }

    /**
     * Set a generic error to show in the view
     *
     * @access  public
     * @param   string A generic error
     */
    public function setError($str)
    {
        if(strlen($str) > 0) {
            $this->session->set_flashdata('error',  $this->session->flashdata('error') . '<p>' . $str . '</p>');
        }
    }

    /**
     * Set a generic msg to show in the view
     *
     * @access  public
     * @param   string A generic msg
     */
    public function setMsg($str)
    {
        if(strlen($str) > 0) {
            $this->session->set_flashdata('msg',  $this->session->flashdata('msg') . '<p>' . $str . '</p>');
        }

    }

    /**
     * Checks if user is logged
     *
     * @access  public
     */
    public function renderer()
    {
        if(isset($this->data['banner']) && is_object($this->data['banner'])) {
                $banner = $this->data['banner'];
                $this->data['banner'] = array();
                $this->data['banner'][] = $banner;
        }
        if(!isset($this->data['hive'])) {
            $this->data['hive'] = $this->router->class . '_' . $this->router->method;
        }
        $this->data['hive'] = 'assets' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'hive' . DIRECTORY_SEPARATOR . $this->data['hive'] . '.png';

        if (file_exists(ROOT_PATH . DIRECTORY_SEPARATOR .  $this->data['hive']) === false) {
            unset($this->data['hive']);
        }else{
            $this->data['hive'] = str_replace('\\', '/', $this->data['hive']);
        }


        $this->loadCss(array(
            'name' => 'main',
            'path' => $this->uri->segment(1) == 'adm' ? 'assets/adm/css/' : null
        ), true);
        $this->loadCss(array(
            'name' => 'media',
            'path' => $this->uri->segment(1) == 'adm' ? 'assets/adm/css/' : null
        ), true);
        $this->loadCss(array(
            'name' => $this->router->class . '_' . $this->router->method,
            'path' => $this->uri->segment(1) == 'adm' ? 'assets/adm/css/' : null
        ));
        $this->loadJs(
            array(
                array(
                    'name' => 'script',
                    'path' => $this->uri->segment(1) == 'adm' ? 'assets/adm/js/' : null
                ),
                array(
                    'name' => $this->router->class . '_' . $this->router->method,
                    'path' => $this->uri->segment(1) == 'adm' ? 'assets/adm/js/' : null
                )
            )
        );
        $this->data['js'] = implode(null, $this->js);
        $this->data['css'] = implode(null, $this->css);
        $this->data['assets'] = implode(null, $this->assets);

        $this->getPageTitle();

        if ($this->header !== NULL) {
            $this->load->view($this->header, $this->data);
        }
        $this->load->view($this->content, $this->data);

        if ($this->footer !== NULL) {
            $this->load->view($this->footer, $this->data);
        }
    }

    public function getPageTitle()
    {
        if (isset($this->data['pageTitle']) == false) {
            $this->setPageTitle(ucfirst($this->router->class));
        }
        return $this->data['pageTitle'];
    }

    /**
     * Set a window title
     *
     * @access  public
     * @param   string A $page name
     */
    public function setPageTitle($page = '', $separator = ' - ')
    {
        $this->data['pageTitle'] = $page . $separator . NAME_SITE;
    }

    /**
     * Set a previous URL
     *
     * @access  public
     * @param   string A valid address internal or external
     */
    public function setPreviousUrl($address)
    {
        if(strpos($address, 'login') === false){
            $previousUrl = $this->session->userdata('previousUrl')?$this->session->userdata('previousUrl'):array();
            $previousUrl[] = $address;
            $this->session->set_userdata('previousUrl',$previousUrl);
        }
    }

    /**
     * Redirect the user, to previous URL
     *
     * @access  public
     */
    public function goToPreviousUrl()
    {
        $previousUrl = $this->session->userdata('previousUrl')?$this->session->userdata('previousUrl'):array();

        if (is_array($previousUrl) && count($previousUrl) > 0) {
            $previous = array_pop($previousUrl);
            $this->session->set_userdata('previousUrl',$previousUrl);
            redirect($previous, 'refresh');
        } else {
            return FALSE;
        }
    }

    protected function loadCss(array $files, $priority = false)
    {
        $version = 3.16;

        if(is_array(current($files)) === false) {
            $files = array($files);
        }
        foreach ($files as $row) {
            if(isset($row['name']) === false) {
                log_message('error', __METHOD__ . ROOT_PATH . DIRECTORY_SEPARATOR . 'css name not defined. Dump: ' . var_export($row, true));
                continue;
            }
            if(isset($row['path']) === false || $row['path'] === null) {
                $row['path'] = 'assets/css/';
            }
            $fileNoMin = $row['path'] . $row['name'] . '.css';
            $fileMin = $row['path'] . $row['name'] . '.min.css';

            if(isset($row['key']) === false) {
                $row['key'] = md5($fileNoMin);
            }
            //Put as the last one if was defined before
            if(isset($this->css[$row['key']]) === true) {
                $data = $this->css[$row['key']];
                unset($this->css[$row['key']]);
                $this->css[$row['key']] = $data;
                continue;
            }
            if (file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . $fileNoMin) === TRUE) {
                if($this->config->item('minify_output') === true) {
                    if (ENVIRONMENT == 'development' ||
                        file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . $fileMin) === FALSE
                        || isset($this->data['adm']->id) === true
                    ) {
                        $this->load->helper('file');
                        $contents = file_get_contents($fileNoMin);
                        $contents = $this->output->minify($contents,'text/css');
                        write_file($fileMin, $contents);
                    }
                }else{
                    @unlink($fileMin);
                    $fileMin = $fileNoMin;
                }
                if($priority === true) {
                    $this->css[$row['key']] = '';
                    $this->assets[$row['key']] = PHP_EOL . '    <link rel="stylesheet" type="text/css" href="./' . $fileMin . '?v=' . $version . '"/>';
                }else{
                    $this->css[$row['key']] = PHP_EOL . '    <link rel="stylesheet" type="text/css" href="./' . $fileMin . '?v=' . $version . '"/>';
                }
            } else {
                log_message('debug', __METHOD__ . ROOT_PATH . DIRECTORY_SEPARATOR . $fileNoMin . ' does not exists');
            }
        }
    }
    protected function loadJs(array $files, $priority = false)
    {
        $version = 3.9;

        if(is_array(current($files)) === false) {
            $files = array($files);
        }
        foreach ($files as $row) {
            if(isset($row['name']) === false) {
                log_message('error', __METHOD__ . ROOT_PATH . DIRECTORY_SEPARATOR . 'js name not defined. Dump: ' . var_export($row, true));
                continue;
            }
            if(isset($row['path']) === false || $row['path'] === null) {
                $row['path'] = 'assets/js/';
            }
            $fileNoMin = $row['path'] . $row['name'] . '.js';
            $fileMin = $row['path'] . $row['name'] . '.min.js';

            if(isset($row['key']) === false) {
                $row['key'] = md5($fileNoMin);
            }
            //Put as the last one if was defined before
            if(isset($this->js[$row['key']]) === true) {
                $data = $this->js[$row['key']];
                unset($this->js[$row['key']]);
                $this->js[$row['key']] = $data;
                continue;
            }
            if (file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . $fileNoMin) === TRUE) {
                if($this->config->item('minify_output') === true) {
                    if (ENVIRONMENT == 'development' ||
                        file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . $fileMin) === FALSE) {
                        $this->load->helper('file');
                        $contents = file_get_contents($fileNoMin);
                        $contents = $this->output->minify($contents, 'text/javascript');
                        write_file($fileMin, $contents);
                    }
                }else{
                    @unlink($fileMin);
                    $fileMin = $fileNoMin;
                }
                if($priority === true) {
                    $this->js[$row['key']] = '';
                    $this->assets[$row['key']] = PHP_EOL . '    <script type="text/javascript" src="./' . $fileMin . '?v=' . $version .  '"></script>';
                }else{
                    $this->js[$row['key']] = PHP_EOL . '    <script type="text/javascript" src="./' . $fileMin . '?v=' . $version . '"></script>';
                }

            } else {
                log_message('debug', __METHOD__ . ROOT_PATH . DIRECTORY_SEPARATOR . $fileNoMin . ' does not exists');
            }
        }
    }
	  private function set_lang()
    {
        $url = current_url();
        $url = str_replace('/index.php', '', $url);
        if ($this->input->get('lang')) {
            $this->input->set_cookie(array(
                'name' => 'lang',
                'value' => $this->input->get('lang'),
                'expire' => (365 * 24 * 60 * 60),
                'path' => '/'
            ));
            unset($_GET['lang']);
            parse_str($_SERVER['QUERY_STRING'], $_GET);
            redirect($url);
        }
        $lang = $this->input->cookie('lang', true);
        if (!$lang) {
            redirect($url . '?lang=portuguese');
        }
        $this->lang->load('site', $lang);

        if(file_exists(APPPATH . 'language/' . $lang .  '/' . $this->router->class . '_' . $this->router->method . '_lang.php')) {
            $this->lang->load($this->router->class . '_' . $this->router->method, $lang);
        }

        return $this->data['lang'] = $lang;
    }
    private function avaliable_lang()
    {
        $avaliable_lang = $this->input->cookie('avaliable_lang', true);

        if (!$avaliable_lang) {

            $avaliable_lang = array();

            $this->load->helper('directory');

            $path = APPPATH . 'language/';
            $map = directory_map($path, 1);

            foreach ($map as $row) {
                if (is_dir($path . $row)) {
                    $avaliable_lang[] = str_replace('/', '', str_replace('\\', '', $row));
                }
            }
            $avaliable_lang = serialize($avaliable_lang);
            $this->input->set_cookie(array(
                'name' => 'avaliable_lang',
                'value' => $avaliable_lang,
                'expire' => (1 * 24 * 60 * 60),
                'path' => '/'
            ));
        }
        return $this->data['avaliable_lang'] = unserialize($avaliable_lang);
    }
}