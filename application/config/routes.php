<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['adm'] = 'adm/home';
$route['adm/login'] = 'adm/login';
$route['adm/login/auth'] = 'adm/login/auth';

$route['adm/admin'] = 'adm/admin';
$route['adm/admin/novo'] = 'adm/admin/edit';
$route['adm/admin/editar/(:num)'] = 'adm/admin/edit/$1';
$route['adm/admin/excluir/(:num)'] = 'adm/admin/delete/$1';
$route['adm/admin/salvar/?(:num)?'] = 'adm/admin/save/$1';

$route['adm/post'] = 'adm/post';
$route['adm/post/novo'] = 'adm/post/edit';
$route['adm/post/editar/(:num)'] = 'adm/post/edit/$1';
$route['adm/post/excluir/(:num)'] = 'adm/post/delete/$1';
$route['adm/post/salvar/?(:num)?'] = 'adm/post/record/$1';

$route['adm/cliente'] = 'adm/client';
$route['adm/cliente/novo'] = 'adm/client/edit';
$route['adm/cliente/editar/(:num)'] = 'adm/client/edit/$1';
$route['adm/cliente/excluir/(:num)'] = 'adm/client/delete/$1';
$route['adm/cliente/salvar/?(:num)?'] = 'adm/client/record/$1';

$route['adm/depoimento'] = 'adm/testimonial';
$route['adm/depoimento/novo'] = 'adm/testimonial/edit';
$route['adm/depoimento/editar/(:num)'] = 'adm/testimonial/edit/$1';
$route['adm/depoimento/excluir/(:num)'] = 'adm/testimonial/delete/$1';
$route['adm/depoimento/salvar/?(:num)?'] = 'adm/testimonial/record/$1';

$route['adm/banner'] = 'adm/banner';
$route['adm/banner/novo'] = 'adm/banner/edit';
$route['adm/banner/editar/(:num)'] = 'adm/banner/edit/$1';
$route['adm/banner/excluir/(:num)'] = 'adm/banner/delete/$1';
$route['adm/banner/salvar/?(:num)?'] = 'adm/banner/record/$1';

$route['adm/midia'] = 'adm/midia';
$route['adm/midia/novo'] = 'adm/midia/edit';
$route['adm/midia/editar/(:num)'] = 'adm/midia/edit/$1';
$route['adm/midia/excluir/(:num)'] = 'adm/midia/delete/$1';
$route['adm/midia/salvar/?(:num)?'] = 'adm/midia/record/$1';

$route['adm/release'] = 'adm/release';
$route['adm/release/novo'] = 'adm/release/edit';
$route['adm/release/editar/(:num)'] = 'adm/release/edit/$1';
$route['adm/release/excluir/(:num)'] = 'adm/release/delete/$1';
$route['adm/release/salvar/?(:num)?'] = 'adm/release/record/$1';

$route['adm/contato'] = 'adm/contact';
$route['adm/contato/novo'] = 'adm/contact/edit';
$route['adm/contato/editar/(:num)'] = 'adm/contact/edit/$1';
$route['adm/contato/excluir/(:num)'] = 'adm/contact/delete/$1';
$route['adm/contato/salvar/?(:num)?'] = 'adm/contact/record/$1';

$route['institucional'] = 'institutional/index';
$route['produtos'] = 'products/index';
$route['clientes'] = 'clients/index';
$route['imprensa'] = 'press/index';
$route['carreira'] = 'career/index';
$route['contato'] = 'contact/index';
$route['contato/enviar'] = 'contact/send';

$route['produtos/logistica'] = 'products/logistic';
$route['produtos/financeira'] = 'products/index';
$route['produtos/inteligencia'] = 'products/intelligence';
$route['produtos/sysdoctor'] = 'products/sysdoctor';
$route['produtos/servicos'] = 'products/service';
$route['produtos/rh'] = 'products/hr';