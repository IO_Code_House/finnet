$(function() {
	var oTable = $('.dataTable').dataTable({
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sZeroRecords': 'Nada encontrado - desculpe',
			'sInfo': 'Mostrando _START_ até _END_ de _TOTAL_ registros',
			'sSearch': 'Procurar:',
			'sInfoEmpty': 'Mostrando 0 até 0 de 0 registros',
			'sInfoFiltered': '(Filtrado de _MAX_ registros)',
			'oPaginate': {
				'sFirst': 'Primeiro',
				'sPrevious': 'Anterior',
				'sNext': 'Próximo',
				'sLast': 'Último'
			}
		},
		'bRetrieve' : true
	});
	$('.dataTable').data('oTable', oTable);
	$('.dataTables_length').css('display','none');
});