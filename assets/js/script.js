$(function () {
    var content = $('meta[name=controller]').prop('content');
    var method = $('meta[name=method]').prop('content');

    $('.' + content + ', .' + content + '_' + method).addClass('active');

    $('.nav-list li a,.list a' ).click(function(e){
        var hash = $(this).prop('href').split('#');
        hash = hash[1];
        try{
            var pageTop = $('#' + hash).offset().top;
        }catch(err){
            console.log(err);
        }
        pageTop = pageTop ? pageTop : 0;//In case any error occur the page will scroll to beguin
        $('html, body').stop().animate({scrollTop:pageTop}, 800);
    });

    // Enabling Popover Example 1 - HTML (content and title from html tags of element)
    $("[data-toggle=popover]").popover();

    /*
    Todo
     */

    $("[data-toggle=popover]").on('click', function (e) {
        $("[data-toggle=popover]").not(this).popover('hide');
    });
    setTimeout(function(){
        $('.animate').addClass('start');
    }, 100);

    var carousel = $('#carousel_banner');
    var qtd = carousel.find('.item').length;
    if(qtd == 1){
        $(carousel).find('.carousel-control').hide();
        $(carousel).find('.carousel-indicators').hide();
    }
    $('#carousel_banner .item').first().addClass('active');
    $('#carousel_banner .carousel-indicators li').first().addClass('active');
});
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});