$(function () {
    $('.testimonial .carousel .item').first().addClass('active');
    $('.testimonial .accordion .collapse').first().addClass('in');
});

$(function () {
    setInterval(function () {
        var next_accordion = $('.testimonial .accordion .in').removeClass('in').next();
        if (next_accordion.html() == undefined) {
            $('.testimonial .accordion .collapse').first().addClass('in');

        } else {
            next_accordion.addClass('in');
        }
    }, 5000);
});